ManticoreDB
===========

A minimalist database for java.

How to use:
```java
// Get a database instance with default settings.
Database db = Database.init();

// Store an object to file.
db.add(user);

// Get an object
User steve = db.select(User.class).where(is("name", "Steve")).getFirst();

// Update an object
db.update(User.class).set("name", "Steveski").where(is("name", "Steve")).execute();

// Get a stream of objects 
Stream<User> programmers = db.select(User.class).where(is("occupation", "Programmer")).stream();
```

## As a Server

Any database instance can be ran as a server easily.

```java
Server server = Database.init().server(8080);
server.start();
```

The code above will allow your application to start listening on the port 8080, servicing all requests.

## As a client

Connecting to a server is just as easy.

```java
Database remoteDB = Database.connect("192.168.0.19:8080");
```

### Query Language

Manticore uses a modified subset of standard SQL for communicating between servers.

#### Select Statement
```
SELECT id, name, occupation FROM users
WHERE occupation is 'Programmer'
ORDER BY name ASC
```

#### Insert Statement
```
INSERT INTO users (id, name, occupation) VALUES 
(123, 'Steve Montgomery', 'Professor')
```

#### Update
```
UPDATE users
SET name='Steve Montgomery', occupation='Professor'
WHERE id=123
```

#### Delete
```
DELETE FROM users
WHERE id=123
```

#### Listen
```
LISTEN TO users (id, name, occupation)
WHERE occupation='Programmer'
ON INSERT
```