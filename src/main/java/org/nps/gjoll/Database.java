package org.nps.gjoll;

import org.nps.gjoll.data.Streams;
import org.nps.gjoll.file.FileTableFactory;
import org.nps.gjoll.meta.TypeContentDescriptor;
import org.nps.gjoll.statements.*;
import org.nps.gjoll.tables.CachingTableFactory;
import org.nps.gjoll.tables.DataTableFactory;

import java.nio.file.Paths;
import java.util.Iterator;
import java.util.stream.Stream;

public interface Database extends DataTableFactory {

    /**
     * Create a database with default settings.
     */
    static Database init() {
        return init(new DatabaseSettings());
    }

    static Database init(DatabaseSettings settings) {
        return new GenericDatabase(new CachingTableFactory(new FileTableFactory(Paths.get(settings.getPath()))));
    }

    default <T> Select<T> select(Class<T> type) {
        return new SelectImpl<>(this, new TypeContentDescriptor<>(type));
    }

    default <T> Update<T> update(Class<T> type) {
        return new UpdateImpl<>(this, new TypeContentDescriptor<>(type));
    }

    default <T> void add(Iterable<? extends T> items) {
        final Iterator<? extends T> iter = items.iterator();
        if (iter.hasNext())
            add((Class<T>) iter.next().getClass(), Streams.toStream(items));
    }
    default <T> void add(T... items) {
        add((Class<T>) items[0].getClass(), Stream.of(items));
    }
    default <T> void add(Class<T> type, Stream<? extends T> items) {
        new InsertImpl<>(this, type, items).execute();
    }
    default <T> Listen<T> listen(Class<T> type) {
        return new ListenImpl<>(this, new TypeContentDescriptor<>(type));
    }
    default <T> Delete<T> delete(Class<T> type) {
        return new DeleteImpl<>(this, new TypeContentDescriptor<>(type));
    }

}