package org.nps.gjoll;

public class DatabaseSettings {

    private String path = "data";

    public String getPath() {
        return path;
    }

    public DatabaseSettings setPath(String path) {
        this.path = path;
        return this;
    }

}
