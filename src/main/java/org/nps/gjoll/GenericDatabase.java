package org.nps.gjoll;

import org.nps.gjoll.statements.ContentLocator;
import org.nps.gjoll.tables.DataTable;
import org.nps.gjoll.tables.DataTableFactory;

public class GenericDatabase implements Database {

    final DataTableFactory tableFactory;

    public GenericDatabase(DataTableFactory tableFactory) {
        this.tableFactory = tableFactory;
    }

    @Override
    public <T> DataTable<T> get(ContentLocator<T> locator) {
        return tableFactory.get(locator);
    }
}