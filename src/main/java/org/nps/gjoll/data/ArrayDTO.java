package org.nps.gjoll.data;

import org.nps.gjoll.meta.FieldInfo;
import org.nps.gjoll.meta.TableDescriptor;

public class ArrayDTO implements DTO {

    final TableDescriptor tableDescriptor;
    final Object[] values;

    public ArrayDTO(TableDescriptor tableDescriptor, Object[] values) {
        this.tableDescriptor = tableDescriptor;
        this.values = values;
    }

    @Override
    public Object get(FieldInfo key) {
        return values[key.getIndex()];
    }

    @Override
    public Object get(String key) {
        return get(tableDescriptor.get(key));
    }

    @Override
    public void set(FieldInfo field, Object value) {
        values[field.getIndex()] = value;
    }
}
