package org.nps.gjoll.data;

import org.nps.gjoll.meta.Primitives;

public class ConversionUtil {

    public static <T> T convertTo(Object o, Class<T> type) {
        try {
            if (o == null)
                return null;
            if (o.getClass().isAssignableFrom(type))
                return type.cast(o);
            else if (type.isPrimitive())
                return convertToPrimitive(o, type);
            else if (Primitives.isWrapperType(type))
                return type.cast(convertToPrimitive(o, Primitives.unwrap(type))); // TODO bug?
            else if (type.equals(String.class))
                return type.cast(String.valueOf(o));
        } catch (ReflectiveOperationException e) {
            throw new ConversionException(e);
        }
        throw new UnsupportedOperationException("Unsupported conversion from "+o.getClass()+" to "+type);
    }

    private static <T> T convertToPrimitive(Object o, Class<T> type) throws InstantiationException, IllegalAccessException, java.lang.reflect.InvocationTargetException, NoSuchMethodException {
        final Class<?> inputType = o.getClass();
        if (inputType.isPrimitive())
            return type.cast(inputType);
        else if (Primitives.isWrapperType(inputType))
            return type.cast(Primitives.unwrap(inputType));
        else if (inputType.equals(String.class))
            return (T) Primitives.wrap(type).getConstructor(String.class).newInstance(o);
        throw new UnsupportedOperationException("Unsupported conversion from "+inputType+" to "+type);
    }

    public static class ConversionException extends RuntimeException {

        public ConversionException(Throwable cause) {
            super(cause);
        }
    }

}
