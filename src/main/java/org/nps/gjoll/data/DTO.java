package org.nps.gjoll.data;

import org.nps.gjoll.meta.FieldInfo;

public interface DTO {

    Object get(String key);
    Object get(FieldInfo key);
    void set(FieldInfo name, Object value);
}
