package org.nps.gjoll.data;

import org.nps.gjoll.meta.FieldInfo;
import org.nps.gjoll.meta.TableDescriptor;
import org.nps.gjoll.query.Pair;

import java.util.Iterator;
import java.util.Map;

public class DTOAssignmentIterator implements Iterator<Map.Entry<FieldInfo, Object>> {

    final TableDescriptor info;
    final DTO dto;

    int index = 0;

    public DTOAssignmentIterator(TableDescriptor info, DTO dto) {
        this.info = info;
        this.dto = dto;
    }

    @Override
    public boolean hasNext() {
        return index < info.getNumberOfFields();
    }

    @Override
    public Map.Entry<FieldInfo, Object> next() {
        final FieldInfo field =  info.get(index++);
        return new Pair<>(field, dto.get(field));
    }
}
