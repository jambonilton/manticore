package org.nps.gjoll.data;

import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * For working with base types.
 */
public class DataTypes {

    private static final Set<Class<?>> TYPES = Stream.of(
            boolean.class, Boolean.class,
            byte.class, Byte.class,
            char.class, Character.class,
            double.class, Double.class,
            float.class, Float.class,
            int.class, Integer.class,
            long.class, Long.class,
            short.class, Short.class,
            void.class, Void.class,
            String.class,
            Date.class,
            Calendar.class,
            Instant.class
    ).collect(Collectors.toSet());

    public static boolean isType(Class<?> type) {
        return TYPES.contains(type);
    }

}
