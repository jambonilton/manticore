package org.nps.gjoll.data;

import org.nps.gjoll.query.Pair;

import java.util.ArrayList;
import java.util.Map;

public class Entries<T> extends ArrayList<Map.Entry<String, T>> {

    public Entries put(String key, T value) {
        add(new Pair<>(key, value));
        return this;
    }

}
