package org.nps.gjoll.data;

import java.util.Iterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class Streams {

    public static <T> Stream<T> toStream(Iterable<? extends T> iterable) {
        return StreamSupport.stream((Spliterator<T>) iterable.spliterator(), false);
    }

    public static <T> Stream<T> toStream(Iterator<? extends T> iterator) {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(iterator, Spliterator.ORDERED), false);
    }

}
