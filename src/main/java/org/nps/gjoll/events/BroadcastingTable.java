package org.nps.gjoll.events;

import org.nps.gjoll.query.Query;
import org.nps.gjoll.query.SortOrder;
import org.nps.gjoll.tables.DataTable;
import org.nps.gjoll.tables.MutableDataTable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class BroadcastingTable<T> implements DataTable<T> {

    final MutableDataTable<T> delegate;

    final List<BiConsumer<T, Iterable<Map.Entry<String, Object>>>> updateListeners = new ArrayList<>();
    final List<Consumer<T>> insertListeners = new ArrayList<>();
    final List<Consumer<T>> deleteListeners = new ArrayList<>();

    public BroadcastingTable(MutableDataTable<T> delegate) {
        this.delegate = delegate;
    }

    @Override
    public void update(Query query, Collection<Map.Entry<String, Object>> assignments) {
        if (!updateListeners.isEmpty())
            delegate.get(query).forEach(e -> updateListeners.forEach(consumer -> consumer.accept(e, assignments)));
        delegate.update(query, assignments);
    }

    @Override
    public void delete(Query query) {
        delegate.delete(query);
        if (!deleteListeners.isEmpty())
            delegate.get(query).forEach(e -> deleteListeners.parallelStream().forEach(consumer -> consumer.accept(e)));
    }

    @Override
    public Stream<T> get(Query query, Iterable<Map.Entry<String, SortOrder>> sorting) {
        return delegate.get(query, sorting);
    }

    @Override
    public void add(Stream<? extends T> items) {
        delegate.add(items.map(this::recordInsert));
    }

    private T recordInsert(T item) {
        insertListeners.parallelStream().forEach(consumer -> consumer.accept(item));
        return item;
    }

    @Override
    public void onUpdate(Query query, BiConsumer<T, Iterable<Map.Entry<String, Object>>> consumer) {
        updateListeners.add((e, changes) -> {
            if (query.test(e))
                consumer.accept(e, changes);
        });
    }

    @Override
    public void onInsert(Query query, Consumer<T> consumer) {
        insertListeners.add((e) -> {
            if (query.test(e))
                consumer.accept(e);
        });
    }

    @Override
    public void onDelete(Query query, Consumer<T> consumer) {
        deleteListeners.add((e) -> {
            if (query.test(e))
                consumer.accept(e);
        });
    }
}
