package org.nps.gjoll.file;

import org.nps.gjoll.index.LongEntry;
import org.nps.gjoll.index.LongIndexData;
import org.nps.gjoll.io.channels.ChannelIterator;
import org.nps.gjoll.io.Format;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class FileLongIndexData<E> implements LongIndexData<E> {

    final File file;
    final FileOutputStream out;
    final FileInputStream in;
    final Format<E> format;

    long size;

    public FileLongIndexData(File file,
                             Format<E> format) throws IOException {
        this.file = file;
        this.out = new FileOutputStream(file, true);
        this.in = new FileInputStream(file);
        this.format = format;
        size = elements().count();
    }

    @Override
    public E get(long index) {
        try {
            FileChannel channel = in.getChannel(); // TODO Closing does not work?
            channel.position(index);
            return format.read(channel);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public synchronized long insert(E item) {
        final long location = file.length();
        add(item);
        return location;
    }

    private void add(E item) {
        FileChannel channel = out.getChannel();
        format.write(channel, item);
        size++;
    }

    @Override
    public Stream<E> elements() {
        try {
            final FileChannel channel = in.getChannel().position(0);
            final Iterator<E> channelIterator = new ChannelIterator(channel, format);
            return StreamSupport.stream(Spliterators.spliteratorUnknownSize(channelIterator,
                    Spliterator.ORDERED | Spliterator.NONNULL), false).onClose(()->closeChannel(channel));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Stream<LongEntry<E>> entries() {
        try {
            final FileChannel channel = in.getChannel().position(0);
            return StreamSupport.stream(Spliterators.spliteratorUnknownSize(new FileEntryIterator(channel),
                    Spliterator.ORDERED | Spliterator.NONNULL), false).onClose(()->closeChannel(channel));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void closeChannel(FileChannel channel) {
        try {
            channel.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    final class FileEntryIterator implements Iterator<LongEntry<E>> {

        final FileChannel fc;

        LongEntry<E> entry;

        public FileEntryIterator(FileChannel fc) {
            this.fc = fc;
            advance();
        }

        private void advance() {
            try {
                final long position = fc.position();
                final E next = format.read(fc);
                entry = next == null ? null : new LongEntry<>(position, next);
            } catch (IOException e) {
                throw new RuntimeException(e); // TODO
            }
        }

        @Override
        public boolean hasNext() {
            return entry != null;
        }

        @Override
        public LongEntry<E> next() {
            try {
                return entry;
            } finally {
                advance();
            }
        }

    }

}
