package org.nps.gjoll.file;

import org.nps.gjoll.events.BroadcastingTable;
import org.nps.gjoll.index.*;
import org.nps.gjoll.io.bson.BinaryObjectFormat;
import org.nps.gjoll.io.bson.FieldInfoFormat;
import org.nps.gjoll.io.bson.TableDescriptorFile;
import org.nps.gjoll.meta.ContentDescriptor;
import org.nps.gjoll.meta.ContentDescriptorAdapter;
import org.nps.gjoll.meta.FieldInfo;
import org.nps.gjoll.meta.TableDescriptor;
import org.nps.gjoll.statements.ContentLocator;
import org.nps.gjoll.tables.DataTable;
import org.nps.gjoll.tables.DataTableFactory;
import org.nps.gjoll.updates.BinaryUpdateFormat;
import org.nps.gjoll.updates.IndexedUpdate;
import org.nps.gjoll.updates.LongIndexMutableTable;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class FileTableFactory implements DataTableFactory {

    public static final String DATA_EXT = ".data",
                               META_EXT = ".meta",
                               UPDATES_EXT = ".changes";

    final Path rootDir;
    final TableDescriptorFile metaIO;

    public FileTableFactory(Path rootDir) {
        this.rootDir = rootDir;
        this.metaIO = new TableDescriptorFile(new FieldInfoFormat());
    }

    @Override
    public <T> DataTable<T> get(ContentLocator<T> locator) {
        try {

            final File dataFile = rootDir.resolve(getFileName(locator, DATA_EXT)).toFile(),
                       metaFile = rootDir.resolve(getFileName(locator, META_EXT)).toFile(),
                       updatesFile = rootDir.resolve(getFileName(locator, UPDATES_EXT)).toFile();

            ContentDescriptor<T> descriptor = locator;
            if (!dataFile.getParentFile().exists())
                dataFile.getParentFile().mkdirs();
            if (!dataFile.exists()) {
                dataFile.createNewFile();
                metaIO.write(metaFile, locator);
            } else {
                final TableDescriptor tableDescriptor = metaIO.read(metaFile);
                descriptor = new ContentDescriptorAdapter<>(descriptor, tableDescriptor);
            }

            final LongIndexMutableTable mutableTable = new LongIndexMutableTable(descriptor,
                    dataTable(descriptor, dataFile),
                    changesTable(descriptor, updatesFile));

            return new BroadcastingTable<>(mutableTable);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private <T> LongIndexDataTable<T> dataTable(ContentDescriptor<T> descriptor, File dataFile) throws IOException {
        final FileLongIndexData<T> data = new FileLongIndexData<>(dataFile, new BinaryObjectFormat(descriptor));
        final LongIndex index = getIndexForContent(descriptor);
        return new LongIndexDataTable<>(data, index);
    }

    private <T> LongIndex getIndexForContent(TableDescriptor descriptor) {
        final List<LongIndex<T>> indices = new ArrayList<>();
        for (FieldInfo field : descriptor) {
            if (field.getType().isAssignableFrom(Collection.class) || field.getType().isArray())
                indices.add(new ContainsIndex<>(field.getName(), new CollectionItemizer()));
            if (field.getType().equals(String.class))
                indices.add(new ContainsIndex<>(field.getName(), new RegexStringItemizer()));
            if (field.getType().isPrimitive() || Comparable.class.isAssignableFrom(field.getType()))
                indices.add(new TreeIndex<>(field.getName()));
        }
        if (!indices.isEmpty())
            return new MultiIndex<>(indices);
        return new NullLongIndex();
    }

    private <T> LongIndexDataTable<IndexedUpdate> changesTable(TableDescriptor descriptor, File updatesFile) throws IOException {
        final FileLongIndexData<IndexedUpdate> data = new FileLongIndexData<>(updatesFile, new BinaryUpdateFormat(descriptor));
        final LongIndex index = new NullLongIndex();
        return new LongIndexDataTable<>(data, index);
    }

    private <T> String getFileName(ContentLocator locator, String extension) {
        return locator.getLabel().replaceAll("\\.", "/") + extension;
    }


}
