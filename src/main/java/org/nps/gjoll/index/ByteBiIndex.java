package org.nps.gjoll.index;

import java.util.Collection;

public interface ByteBiIndex<E> {

    E get(byte index);

    byte get(E item);

    Collection<E> values();

    int size();

}
