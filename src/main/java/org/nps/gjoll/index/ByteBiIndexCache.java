package org.nps.gjoll.index;

import com.carrotsearch.hppc.ObjectByteMap;
import com.carrotsearch.hppc.ObjectByteOpenHashMap;

import java.util.Collection;
import java.util.stream.Collectors;

public class ByteBiIndexCache<E> implements ByteBiIndex<E> {

    final ByteIndexData<E> base;
    final ObjectByteMap<E> map;

    public ByteBiIndexCache(ByteIndexData<E> base) {
        this(base, new ObjectByteOpenHashMap<>());
    }

    public ByteBiIndexCache(ByteIndexData<E> base, ObjectByteMap<E> map) {
        this.base = base;
        this.map = map;
        base.entries().forEach(e -> map.put(e.getValue(), e.getKey()));
    }

    @Override
    public byte get(E e) {
        if (!map.containsKey(e)) {
            final byte key = base.insert(e);
            map.put(e, key);
            return key;
        }
        return map.get(e);
    }

    @Override
    public E get(byte index) {
        return base.get(index);
    }

    @Override
    public Collection<E> values() {
        return base.stream().collect(Collectors.toList());
    }

    @Override
    public int size() {
        return map.size();
    }

}
