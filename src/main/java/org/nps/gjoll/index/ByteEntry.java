package org.nps.gjoll.index;

public class ByteEntry<E> {

    final byte key;
    final E value;

    public ByteEntry(byte key, E value) {
        this.key = key;
        this.value = value;
    }

    public byte getKey() {
        return key;
    }

    public E getValue() {
        return value;
    }

}
