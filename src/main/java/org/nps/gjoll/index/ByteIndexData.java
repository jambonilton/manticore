package org.nps.gjoll.index;

import java.util.stream.Stream;

public interface ByteIndexData<E> {

    E get(byte index);

    byte insert(E item);

    Stream<E> stream();

    Stream<ByteEntry<E>> entries();

}
