package org.nps.gjoll.index;

import com.carrotsearch.hppc.ByteLongMap;
import com.carrotsearch.hppc.ByteLongOpenHashMap;

import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Translates sparse long data into incremental byte data.
 */
public class ByteLongIncrementIndex<E> implements ByteIndexData<E> {

    final LongIndexData<E> longIndexData;
    final ByteLongMap map;

    byte increment = 0;

    public ByteLongIncrementIndex(LongIndexData<E> longIndexData) {
        this.longIndexData = longIndexData;
        this.map = new ByteLongOpenHashMap();
        longIndexData.entries().forEach((e)->map.put(increment++, e.getKey()));
    }

    @Override
    public E get(byte index) {
        if (!map.containsKey(index))
            return null;
        return longIndexData.get(map.get(index));
    }

    @Override
    public synchronized byte insert(E item) {
        map.put(increment++, longIndexData.insert(item));
        return increment;
    }

    @Override
    public Stream<E> stream() {
        return longIndexData.elements();
    }

    @Override
    public Stream<ByteEntry<E>> entries() {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(map.iterator(),
                Spliterator.ORDERED | Spliterator.NONNULL), false).map(e -> new ByteEntry<>(e.key, longIndexData.get(e.value)));
    }
}
