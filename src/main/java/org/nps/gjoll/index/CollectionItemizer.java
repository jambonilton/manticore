package org.nps.gjoll.index;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class CollectionItemizer implements Itemizer {

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public Stream<Object> items(Object arg) {
        if (arg == null)
            return Stream.empty();
        else if (arg instanceof Iterable)
            return StreamSupport.stream(((Iterable) arg).spliterator(), false);
        else if (arg.getClass().isArray())
            return Stream.of((Object[]) arg);
        return Stream.of(arg);
    }

}
