package org.nps.gjoll.index;

import org.nps.gjoll.query.Query;

import java.util.stream.LongStream;

public class IndexResult {

    final boolean hit;
    final Query remainingClauses;
    final LongStream results;

    public IndexResult(boolean hit, Query remainingClauses, LongStream results) {
        this.hit = hit;
        this.remainingClauses = remainingClauses;
        this.results = results;
    }

    public boolean hit() {
        return hit;
    }

    public Query diff() {
        return remainingClauses;
    }

    public LongStream stream() {
        return results;
    }
    
}
