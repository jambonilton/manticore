package org.nps.gjoll.index;

import java.util.stream.Stream;

public interface Itemizer {

    Stream<Object> items(Object arg);
    
}
