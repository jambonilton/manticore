package org.nps.gjoll.index;

public interface LongBiIndex<E> {

    E get(long index);

    long get(E item);

}
