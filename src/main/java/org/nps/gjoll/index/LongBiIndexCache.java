package org.nps.gjoll.index;

import com.carrotsearch.hppc.ObjectLongMap;
import com.carrotsearch.hppc.ObjectLongOpenHashMap;

public class LongBiIndexCache<E> implements LongBiIndex<E> {

    final LongIndexData<E> base;
    final ObjectLongMap<E> map;

    public LongBiIndexCache(LongIndexData<E> base) {
        this(base, new ObjectLongOpenHashMap<>());
    }

    public LongBiIndexCache(LongIndexData<E> base, ObjectLongMap<E> map) {
        this.base = base;
        this.map = map;
    }

    @Override
    public long get(E e) {
        if (!map.containsKey(e)) {
            final long key = base.insert(e);
            map.put(e, key);
            return key;
        }
        return map.get(e);
    }

    @Override
    public E get(long index) {
        return base.get(index);
    }

}
