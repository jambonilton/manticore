package org.nps.gjoll.index;

public class LongEntry<E> {
    final long key;
    final E value;

    public LongEntry(long key, E value) {
        this.key = key;
        this.value = value;
    }

    public long getKey() {
        return key;
    }

    public E getValue() {
        return value;
    }
}
