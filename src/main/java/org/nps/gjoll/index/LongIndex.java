package org.nps.gjoll.index;

import org.nps.gjoll.query.Query;

public interface LongIndex<E> {

    IndexResult get(Query query);
    
    void add(E item, long location);
    
}
