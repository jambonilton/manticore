package org.nps.gjoll.index;

import java.util.stream.Stream;

public interface LongIndexData<E> {

    E get(long index);

    long insert(E item);

    Stream<E> elements();

    Stream<LongEntry<E>> entries();

}
