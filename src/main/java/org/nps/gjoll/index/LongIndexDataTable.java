package org.nps.gjoll.index;

import org.nps.gjoll.query.Query;
import org.nps.gjoll.query.SortOrder;
import org.nps.gjoll.reflect.FieldExtractor;
import org.nps.gjoll.tables.ReadOnlyDataTable;

import java.util.Comparator;
import java.util.Map;
import java.util.stream.Stream;

public class LongIndexDataTable<E> implements ReadOnlyDataTable<E> {

    final LongIndexData<E> data;
    final LongIndex<E> index;

    public LongIndexDataTable(LongIndexData<E> data, LongIndex<E> index) {
        this.data = data;
        this.index = index;
        data.entries().forEach(e -> index.add(e.getValue(), e.getKey()));
    }

    @Override
    public Stream<E> get(Query query, Iterable<Map.Entry<String, SortOrder>> sorting) {
        Stream<E> stream;
        if (query != null) {
            IndexResult indexResult = index.get(query);
            stream = indexResult.hit() ? indexResult.stream().mapToObj(data::get) : data.elements();
            stream = stream.filter(indexResult.diff());
        } else {
            stream = data.elements();
        }
        if (sorting != null)
            stream = stream.sorted(makeComparator(sorting)); // TODO perform sorting with index
        return stream;
    }

    public Stream<LongEntry<E>> getEntries(Query query, Iterable<Map.Entry<String, SortOrder>> sorting) {
        Stream<LongEntry<E>> stream;
        if (query != null) {
            IndexResult indexResult = index.get(query);
            stream = indexResult.hit()
                    ? indexResult.stream().mapToObj(l -> new LongEntry<>(l, data.get(l)))
                    : data.entries();
            stream = stream.filter(e -> indexResult.diff().test(e.getValue()));
        } else {
            stream = data.entries();
        }
        if (sorting != null)
            stream = stream.sorted(makeEntryComparator(sorting)); // TODO perform sorting with index
        return stream;
    }

    private Comparator<? super E> makeComparator(Iterable<Map.Entry<String, SortOrder>> sorting) {
        return (a,b)-> {
            for (Map.Entry<String, SortOrder> sort : sorting) {
                final int comparison = ((Comparable) FieldExtractor.get(a, sort.getKey())).compareTo(FieldExtractor.get(b, sort.getKey()));
                if (comparison != 0)
                    return sort.getValue().equals(SortOrder.DESC) ? -1 * comparison : comparison;
            }
            return 0;
        };
    }

    private Comparator<LongEntry<E>> makeEntryComparator(Iterable<Map.Entry<String, SortOrder>> sorting) {
        final Comparator<? super E> valueComparator = makeComparator(sorting);
        return (e1, e2) -> valueComparator.compare(e1.getValue(), e2.getValue());
    }

    @Override
    public void add(Stream<? extends E> items) {
        items.forEach(item -> index.add(item, data.insert(item)));
    }

}
