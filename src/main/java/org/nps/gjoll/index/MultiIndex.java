package org.nps.gjoll.index;

import com.carrotsearch.hppc.LongOpenHashSet;
import org.nps.gjoll.query.Query;

import java.util.List;
import java.util.stream.StreamSupport;

public class MultiIndex<E> implements LongIndex<E> {

    final List<LongIndex<E>> indices;

    public MultiIndex(List<LongIndex<E>> indices) {
        this.indices = indices;
    }

    @Override
    public IndexResult get(Query query) {
        LongOpenHashSet set = null; // TODO use merged, sorted streams for better performance.
        Query diff = query;
        IndexResult result = null;
        for (LongIndex index : indices) {
            result = index.get(diff);
            if (result.hit()) {
                if (set == null)
                    set = toHashSet(result);
                else
                    set.retainAll(toHashSet(result));
                diff = result.diff();
            }
        }
        if (set == null)
            return new IndexResult(false, query, result.stream());
        return new IndexResult(true, diff, StreamSupport.stream(set.spliterator(), false).mapToLong((c) -> c.value));
    }

    private LongOpenHashSet toHashSet(IndexResult result) {
        final LongOpenHashSet subset = new LongOpenHashSet();
        result.stream().forEach(subset::add);
        return subset;
    }

    @Override
    public void add(E item, long location) {
        for (LongIndex index : indices)
            index.add(item, location);
    }

}
