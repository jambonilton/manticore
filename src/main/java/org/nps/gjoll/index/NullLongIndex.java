package org.nps.gjoll.index;

import org.nps.gjoll.query.Query;

import java.util.stream.LongStream;

public class NullLongIndex implements LongIndex {
    @Override
    public IndexResult get(Query query) {
        return new IndexResult(false, query, LongStream.empty());
    }

    @Override
    public void add(Object item, long location) {
        // no-op
    }
}
