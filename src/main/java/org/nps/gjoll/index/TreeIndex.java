package org.nps.gjoll.index;

import com.carrotsearch.hppc.LongArrayList;
import org.nps.gjoll.index.ranges.Range;
import org.nps.gjoll.query.*;
import org.nps.gjoll.reflect.FieldExtractor;

import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.stream.LongStream;

public class TreeIndex<E, T extends Comparable<T>> implements LongIndex<E> {

    final String field;
    final TreeIndexMap map;

    public TreeIndex(String field) {
        this.field = field;
        this.map = new TreeIndexMap();
    }

    @Override
    public IndexResult get(Query query) {
        final RangeQuery rangeQuery = toRangeQuery(query);
        final LongStream results = map.get(rangeQuery.getRange());
        return new IndexResult(!query.equals(rangeQuery.getDiff()), rangeQuery.getDiff(), results);
    }

    private TreeIndex<E,T>.RangeQuery toRangeQuery(Query query) {
        if (query instanceof Conjunction) {
            final RangeQuery rq = new RangeQuery();
            for (Query sub : query)
                rq.and(toRangeQuery(sub));
            return rq;
        } else if (query instanceof Disjunction) {
            final RangeQuery rq = new RangeQuery();
            for (Query sub : query)
                rq.or(toRangeQuery(sub));
            return rq;
        } else if (query instanceof Comparison && ((Comparison) query).getKey().equals(field)) {
            final Optional<Range<T>> range = Range.fromComparison((Comparison) query);
            if (range.isPresent())
                return new RangeQuery(range.get());
        }
        return new RangeQuery(query);
    }

    @Override
    public void add(E item, long location) {
        final T key = (T) FieldExtractor.get(item, field);
        map.put(key, location);
    }

    private class RangeQuery implements BooleanFormula<RangeQuery> {

        Range<T> range;
        Query diff;

        RangeQuery() {
        }

        RangeQuery(Range<T> range) {
            this.range = range;
        }

        RangeQuery(Query diff) {
            this.diff = diff;
        }

        @Override
        public TreeIndex<E,T>.RangeQuery and(TreeIndex<E,T>.RangeQuery other) {
            and(other.range);
            and(other.diff);
            return this;
        }

        void and(Range<T> other) {
            if (!this.hasRange())
                range = other;
            else if (other != null)
                range = range.and(other);
        }

        boolean hasRange() {
            return range != null;
        }

        void and(Query other) {
            if (!this.hasDiff())
                diff = other;
            else if (other != null)
                diff = diff.and(other);
        }

        boolean hasDiff() {
            return diff != null;
        }

        @Override
        public TreeIndex<E,T>.RangeQuery or(TreeIndex<E,T>.RangeQuery other) {
            or(other.range);
            or(other.diff);
            return this;
        }

        void or(Range<T> other) {
            if (!this.hasRange())
                range = other;
            else if (other != null)
                range = range.or(other);
        }

        void or(Query other) {
            if (!this.hasDiff())
                diff = other;
            else if (other != null)
                diff = diff.or(other);
        }

        Range<T> getRange() {
            if (this.hasRange())
                return range;
            return Range.all();
        }

        Query getDiff() {
            if (this.hasDiff())
                return diff;
            return Query.alwaysTrue();
        }

    }

    private class TreeIndexMap {

        final NavigableMap<T, LongArrayList> base;
        LongArrayList nullItems = new LongArrayList();

        TreeIndexMap() {
            this.base = new ConcurrentSkipListMap<>();
        }

        void put(T key, Long value) {
            if (key == null)
                nullItems.add(value);
            else {
                LongArrayList c = base.get(key);
                if (c == null) {
                    c = new LongArrayList();
                    base.put(key, c);
                }
                c.add(value);
            }
        }

        LongStream get(Range<T> range) {
            final Set<Entry<T, LongArrayList>> entries = range.get(base);
            return entries.stream().flatMapToLong((e) -> {
                final LongArrayList value = e.getValue();
                return LongStream.of(value.toArray());
            });
        }

    }

}
