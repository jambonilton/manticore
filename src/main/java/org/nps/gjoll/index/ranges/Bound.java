package org.nps.gjoll.index.ranges;

class Bound<T> {
	
	final T value;
	final boolean inclusive;
	
	public static <U> Bound<U> empty() {
	    return new Bound<>(null, false);
	}

    public static <U> Bound<U> of(U value) {
        return of(value, true);
    }
    
    public static <U> Bound<U> of(U value, boolean inclusive) {
        return new Bound<>(value, inclusive);
    }
	
	Bound(T value, boolean inclusive) {
		this.value = value;
		this.inclusive = inclusive;
	}

	public T get() {
		return value;
	}

	public boolean isInclusive() {
		return inclusive;
	}
	
	public boolean isSet() {
		return value != null;
	}
	
}
