package org.nps.gjoll.index.ranges;

import java.util.Collections;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.Set;

public class EmptyRange<T extends Comparable<T>> implements Range<T> {

    @Override
    public boolean test(T t) {
        return false;
    }

    // TODO differentiate unbounded limits and impossible limits.
    @Override
    public Bound<T> lowerBound() {
        return Bound.empty();
    }

    // TODO differentiate unbounded limits and impossible limits.
    @Override
    public Bound<T> upperBound() {
        return Bound.empty();
    }

    @Override
    public boolean contains(Range<T> range) {
        return false;
    }

    @Override
    public Range<T> and(Range<T> range) {
        return this;
    }

    @Override
    public Range<T> or(Range<T> range) {
        return range;
    }

    @Override
    public boolean isContinuous() {
        return true;
    }

    @Override
    public boolean isNull() {
        return true;
    }

    @Override
    public <V> Set<Entry<T, V>> get(NavigableMap<T, V> map) {
        return Collections.emptySet();
    }

}
