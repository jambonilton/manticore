package org.nps.gjoll.index.ranges;

import java.util.Collections;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.Set;

/**
 * Represents a continuous interval of values.
 */
public class EnclosedRange<T extends Comparable<T>> implements Range<T> {

    final Bound<T> lowerBound, upperBound;

    public EnclosedRange(Bound<T> lowerBound, Bound<T> upperBound) {
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
    }

    @Override
    public Bound<T> lowerBound() {
        return lowerBound;
    }

    @Override
    public Bound<T> upperBound() {
        return upperBound;
    }

    @Override
    public boolean test(T t) {
        return matchesLower(t) && matchesUpper(t);
    }

    private boolean matchesLower(T t) {
        if (!lowerBound.isSet())
            return true;
        if (lowerBound.isInclusive())
            return lowerBound.get().compareTo(t) >= 0;
        return lowerBound.get().compareTo(t) > 0;
    }

    private boolean matchesUpper(T t) {
        if (!upperBound.isSet())
            return true;
        if (upperBound.isInclusive())
            return upperBound.get().compareTo(t) <= 0;
        return upperBound.get().compareTo(t) < 0;
    }

    @Override
    public boolean contains(Range<T> range) {
        return containsLower(range.lowerBound()) && containsUpper(range.upperBound());
    }

    private boolean containsUpper(Bound<T> other) {
        if (upperBound.isSet()) {
            if (other.isSet())
                return upperBound.get().compareTo(other.get()) >= 0;
            return false;
        }
        return true;
    }

    private boolean containsLower(Bound<T> other) {
        if (lowerBound.isSet()) {
            if (other.isSet())
                return lowerBound.get().compareTo(other.get()) <= 0;
            return false;
        }
        return true;
    }

    public Range<T> and(Range<T> range) {
        if (range.isNull())
            return range;
        else if (range.isContinuous())
            return andContinuous(range);
        else
            return range.and(this);
    }

    private Range<T> andContinuous(Range<T> range) {
        final Bound<T> lower = Range.max(lowerBound(), range.lowerBound()), upper = Range.min(upperBound(), range.upperBound());
        return new EnclosedRange<>(lower, upper);
    }

    @Override
    public boolean isContinuous() {
        return true;
    }

    @Override
    public Range<T> or(Range<T> range) {
        if (this.contains(range))
            return this;
        else if (range.contains(this))
            return range;
        else if (overlapsWith(range))
            return orContinuous(range);
        return new SparseRange<>(this, range);
    }

    private boolean overlapsWith(Range<T> range) {
        if (!range.isContinuous())
            return false;
        return !andContinuous(range).isNull();
    }

    private Range<T> orContinuous(Range<T> range) {
        final Bound<T> lower = Range.min(lowerBound(), range.lowerBound()), upper = Range.max(upperBound(), range.upperBound());
        return new EnclosedRange<>(lower, upper);
    }

    @Override
    public boolean isNull() {
        return lowerBound.isSet() && upperBound.isSet() && lowerBound.get().compareTo(upperBound.get()) > 0;
    }

    @Override
    public <V> Set<Entry<T, V>> get(NavigableMap<T, V> map) {
        if (this.isNull())
            return Collections.emptySet();
        else if (lowerBound.isSet()) {
            if (upperBound.isSet())
                return map.subMap(lowerBound.get(), lowerBound.isInclusive(), upperBound.get(), upperBound.isInclusive()).entrySet();
            return map.tailMap(lowerBound.get(), lowerBound.isInclusive()).entrySet();
        } else if (upperBound.isSet()) {
            return map.headMap(upperBound.get(), upperBound.isInclusive()).entrySet();
        } else {
            return map.entrySet();
        }
    }

}
