package org.nps.gjoll.io;

import java.nio.channels.ByteChannel;

public interface Deserializer<T> {

    /**
     * Reads in an object from the supplied file channel.
     *
     * Returns null if file channel is at EOF.
     */
    T read(ByteChannel in);

}
