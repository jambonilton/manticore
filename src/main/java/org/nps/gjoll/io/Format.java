package org.nps.gjoll.io;

public interface Format<T> extends Deserializer<T>, Serializer<T> {
}
