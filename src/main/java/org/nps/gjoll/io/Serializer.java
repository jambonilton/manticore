package org.nps.gjoll.io;

import java.nio.channels.ByteChannel;

public interface Serializer<T> {

    void write(ByteChannel out, T value);

}
