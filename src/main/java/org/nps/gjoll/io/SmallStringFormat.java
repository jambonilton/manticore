package org.nps.gjoll.io;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.util.Arrays;

/**
 * Writes small strings to byte channels.
 */
public class SmallStringFormat implements Format<String> {

    final ByteBuffer buffer;

    public SmallStringFormat() {
        this(Byte.MAX_VALUE);
    }

    public SmallStringFormat(int size) {
        this.buffer = ByteBuffer.allocate(size);
    }

    @Override
    public String read(ByteChannel in) {
        try {
            final int size = getSize(in);
            if (size == -1)
                return null;
            buffer.clear().limit(size);
            in.read(buffer);
            return new String(Arrays.copyOfRange(buffer.array(), 0, buffer.position()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private int getSize(ByteChannel in) throws IOException {
        buffer.clear().limit(1);
        final int bytes = in.read(buffer);
        buffer.clear();
        return bytes < 0 ? -1 : buffer.get();
    }

    @Override
    public void write(ByteChannel out, String value) {
        try {
            buffer.clear();
            if (value.length() > buffer.capacity() || value.length() > Byte.MAX_VALUE)
                throw new IllegalArgumentException("String "+value+" is too long!");
            buffer.put((byte) value.length());
            buffer.put(value.getBytes());
            buffer.flip();
            out.write(buffer);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
