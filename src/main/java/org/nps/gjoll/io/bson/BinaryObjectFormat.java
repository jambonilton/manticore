package org.nps.gjoll.io.bson;

import org.nps.gjoll.io.Format;
import org.nps.gjoll.io.channels.ChannelReader;
import org.nps.gjoll.io.channels.ChannelWriter;
import org.nps.gjoll.meta.ContentDescriptor;
import org.nps.gjoll.meta.FieldInfo;
import org.nps.gjoll.meta.Primitives;
import org.nps.gjoll.meta.TableDescriptor;
import org.nps.gjoll.query.Pair;

import java.io.IOException;
import java.nio.channels.ByteChannel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

// TODO support for changes in classes
// TODO support for collections, arrays
public class BinaryObjectFormat<T> implements Format<T> {

    final ContentDescriptor<T> contentDescriptor;

    public BinaryObjectFormat(ContentDescriptor<T> contentDescriptor) {
        this.contentDescriptor = contentDescriptor;
    }

    @Override
    public T read(ByteChannel in) {
        try {
            final ChannelReader reader = new ChannelReader(in);
            return readObject(reader);
        } catch (IOException | ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }
    }

    private T readObject(ChannelReader reader) throws IOException, ReflectiveOperationException {
        final int size = reader.read();
        if (size <= 0)
            return null;
        final Iterable<Map.Entry<FieldInfo, Object>> assignments = () -> new FieldValueIterator(contentDescriptor, size, reader);
        return contentDescriptor.create(assignments);
    }

    @Override
    public void write(ByteChannel out, T value) {
        try {
            final ChannelWriter writer = new ChannelWriter(out);
            writeObject(writer, value);
            writer.flush();
        } catch (IOException | ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }
    }

    protected void writeObject(ChannelWriter writer, T value) throws IOException, ReflectiveOperationException {

        final List<Map.Entry<FieldInfo, Object>> entries = new ArrayList<>();
        for (Map.Entry<FieldInfo, Object> assignment : contentDescriptor.iterate(value)) {
            if (assignment.getValue() != null)
                entries.add(assignment);
        }
        writer.put((byte) entries.size());
        for (Map.Entry<FieldInfo, Object> entry : entries) {
            writer.put((byte) entry.getKey().getIndex());
            writeValue(writer, entry.getValue());
        }
    }

    // TODO primitive support
    // TODO DateTime support etc
    public static void writeValue(ChannelWriter writer, Object value) throws IOException, ReflectiveOperationException {
        if (value instanceof String)
            writer.putString((String) value);
        else if (value instanceof Integer)
            writer.putInt((Integer) value);
        else if (value instanceof Long)
            writer.putLong((Long) value);
        else if (value instanceof Double)
            writer.putDouble((Double) value);
        else if (value instanceof Float)
            writer.putFloat((Float) value);
        else if (value instanceof Boolean)
            writer.putBoolean((Boolean) value);
        else if (value instanceof Byte)
            writer.put((Byte) value);
        else if (value instanceof Short)
            writer.putShort((Short) value);
        else if (value instanceof Character)
            writer.putChar((Character) value);
        else
            throw new IllegalArgumentException("Unknown primitive "+value);
    }

    public static class FieldValueIterator implements Iterator<Map.Entry<FieldInfo, Object>> {

        final TableDescriptor tableInfo;
        final int size;
        final ChannelReader reader;

        int count = 0;

        public FieldValueIterator(TableDescriptor tableInfo, int size, ChannelReader reader) {
            this.tableInfo = tableInfo;
            this.size = size;
            this.reader = reader;
        }

        @Override
        public boolean hasNext() {
            return count < size;
        }

        @Override
        public Map.Entry<FieldInfo, Object> next() {
            try {
                count++;
                final byte index = reader.read();
                final FieldInfo fieldInfo = tableInfo.get(index);
                return new Pair<>(fieldInfo, readValue(reader, fieldInfo.getType()));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        public <V> V readValue(ChannelReader in, Class<V> type) throws IOException, ReflectiveOperationException {
            type = Primitives.wrap(type);
            if (type.equals(Integer.class))
                return type.cast(in.read(Integer.BYTES).getInt());
            else if (type.equals(Long.class))
                return type.cast(in.read(Long.BYTES).getLong());
            else if (type.equals(Double.class))
                return type.cast(in.read(Double.BYTES).getDouble());
            else if (type.equals(String.class))
                return type.cast(in.readString(in.read()));
            else if (type.equals(Float.class))
                return type.cast(in.read(Float.BYTES).getFloat());
            else if (type.equals(Short.class))
                return type.cast(in.read(Short.BYTES).getShort());
            else if (type.equals(Boolean.class))
                return type.cast(in.read() != 0);
            else if (type.equals(Byte.class))
                return type.cast(in.read());
            else if (type.equals(Character.class))
                return type.cast(in.read(Character.BYTES).getChar());
            else
                throw new IllegalArgumentException("Unknown primitive "+type);
        }

    }

}
