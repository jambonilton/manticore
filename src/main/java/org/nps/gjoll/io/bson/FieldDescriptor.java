package org.nps.gjoll.io.bson;

import java.util.stream.Stream;

public class FieldDescriptor {

    public static Stream<FieldDescriptor> generate(Class<?> type) {
        return Stream.of(type.getDeclaredFields()).map(f -> new FieldDescriptor(f.getName(), f.getType()));
    }

    final String name;
    final Class<?> type;

    public FieldDescriptor(String name, Class<?> type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public Class<?> getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FieldDescriptor)) return false;

        FieldDescriptor that = (FieldDescriptor) o;

        if (!name.equals(that.name)) return false;
        return type.equals(that.type);

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + type.hashCode();
        return result;
    }
}
