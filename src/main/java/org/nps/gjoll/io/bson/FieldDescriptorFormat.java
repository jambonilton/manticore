package org.nps.gjoll.io.bson;

import org.nps.gjoll.io.channels.ChannelReader;
import org.nps.gjoll.io.channels.ChannelWriter;
import org.nps.gjoll.io.Format;
import org.nps.gjoll.meta.Primitives;

import java.io.IOException;
import java.nio.channels.ByteChannel;

public class FieldDescriptorFormat implements Format<FieldDescriptor> {

    private static final String DELIMITER = ":";

    @Override
    public FieldDescriptor read(ByteChannel in) {
        try {
            final String str = new ChannelReader(in).readSizedString();
            if (str == null || str.isEmpty())
                return null;
            final String[] split = str.split(DELIMITER);
            final FieldDescriptor descriptor = new FieldDescriptor(split[0], getType(split[1]));
            return descriptor;
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    private Class<?> getType(String name) throws ClassNotFoundException {
        switch (name) {
            case "int": return int.class;
            case "long": return long.class;
            case "double": return double.class;
            case "float": return float.class;
            case "byte": return byte.class;
            case "boolean": return boolean.class;
            case "short": return short.class;
            case "char": return char.class;
            default: return Class.forName(name);
        }
    }

    @Override
    public void write(ByteChannel out, FieldDescriptor value) {
        try {
            final ChannelWriter channelWriter = new ChannelWriter(out);
            channelWriter.putString(value.getName()+DELIMITER+getNonPrimitiveType(value).getName());
            channelWriter.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Class<?> getNonPrimitiveType(FieldDescriptor value) {
        final Class<?> type = value.getType();
        if (type.isPrimitive())
            return Primitives.wrap(type);
        return type;
    }

}
