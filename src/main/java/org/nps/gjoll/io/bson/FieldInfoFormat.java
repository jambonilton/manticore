package org.nps.gjoll.io.bson;

import org.nps.gjoll.data.DTO;
import org.nps.gjoll.io.Format;
import org.nps.gjoll.io.channels.ChannelReader;
import org.nps.gjoll.io.channels.ChannelWriter;
import org.nps.gjoll.meta.FieldInfo;
import org.nps.gjoll.meta.Primitives;

import java.io.IOException;
import java.nio.channels.ByteChannel;

public class FieldInfoFormat implements Format<FieldInfo> {

    private static final String DELIMITER = ":";

    @Override
    public FieldInfo read(ByteChannel in) {
        try {
            final String str = new ChannelReader(in).readSizedString();
            if (str == null || str.isEmpty())
                return null;
            final String[] split = str.split(DELIMITER);
            return new FieldInfoImpl(Integer.parseInt(split[0]), split[1], getType(split[2]));
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void write(ByteChannel out, FieldInfo value) {
        try {
            final ChannelWriter channelWriter = new ChannelWriter(out);
            channelWriter.putString(value.getIndex()+DELIMITER+value.getName()+DELIMITER+value.getType().getName());
            channelWriter.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Class<?> getType(String name) throws ClassNotFoundException {
        switch (name) {
            case "int": return int.class;
            case "long": return long.class;
            case "double": return double.class;
            case "float": return float.class;
            case "byte": return byte.class;
            case "boolean": return boolean.class;
            case "short": return short.class;
            case "char": return char.class;
            default: return Class.forName(name);
        }
    }

    private Class<?> getNonPrimitiveType(FieldInfo value) {
        final Class<?> type = value.getType();
        if (type.isPrimitive())
            return Primitives.wrap(type);
        return type;
    }

    static class FieldInfoImpl implements FieldInfo {

        final int index;
        final String name;
        final Class type;

        public FieldInfoImpl(int index, String name, Class type) {
            this.index = index;
            this.name = name;
            this.type = type;
        }

        @Override
        public int getIndex() {
            return index;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public Class getType() {
            return type;
        }

        @Override
        public Object get(Object arg) {
            if (arg instanceof DTO)
                return ((DTO) arg).get(name);
            // TODO
            return null;
        }

        @Override
        public void set(Object arg, Object value) {
            if (arg instanceof DTO)
                ((DTO) arg).set(this, value);
            // TODO
        }
    }

}
