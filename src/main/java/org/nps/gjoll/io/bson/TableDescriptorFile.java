package org.nps.gjoll.io.bson;

import org.nps.gjoll.meta.FieldInfo;
import org.nps.gjoll.meta.FieldInfoDescriptor;
import org.nps.gjoll.meta.TableDescriptor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

public class TableDescriptorFile {

    final FieldInfoFormat fieldInfoFormat;

    public TableDescriptorFile(FieldInfoFormat fieldInfoFormat) {
        this.fieldInfoFormat = fieldInfoFormat;
    }

    public void write(File file, TableDescriptor content) throws IOException {
        try (FileOutputStream out = new FileOutputStream(file)) {
            try (FileChannel channel = out.getChannel()) {
                content.forEach(field -> fieldInfoFormat.write(channel, field));
            }
        }
    }

    public TableDescriptor read(File file) throws IOException {
        final List<FieldInfo> fields = new ArrayList<>();
        try (FileInputStream in = new FileInputStream(file)) {
            try (FileChannel channel = in.getChannel()) {
                FieldInfo field;
                while ((field = fieldInfoFormat.read(channel)) != null)
                    fields.add(field);
            }
        }
        return new FieldInfoDescriptor(fields);
    }

}
