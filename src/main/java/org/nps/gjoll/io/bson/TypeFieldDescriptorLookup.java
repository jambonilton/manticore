package org.nps.gjoll.io.bson;

import org.nps.gjoll.index.ByteBiIndex;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TypeFieldDescriptorLookup implements ByteBiIndex<FieldDescriptor> {

    final Field[] fields;

    public TypeFieldDescriptorLookup(Class<?> type) {
        this(type.getDeclaredFields());
    }

    public TypeFieldDescriptorLookup(Field[] fields) {
        this.fields = fields;
    }

    @Override
    public FieldDescriptor get(byte index) {
        final Field field = fields[index];
        return asFieldDescriptor(field);
    }

    @Override
    public byte get(FieldDescriptor fd) {
        for (int i = 0; i < fields.length; i++)
            if (fd.equals(asFieldDescriptor(fields[i])))
                return (byte) i;
        return -1;
    }

    @Override
    public Collection<FieldDescriptor> values() {
        return Stream.of(fields).map(this::asFieldDescriptor).collect(Collectors.toList());
    }

    @Override
    public int size() {
        return fields.length;
    }

    private FieldDescriptor asFieldDescriptor(Field field) {
        return new FieldDescriptor(field.getName(), field.getType());
    }

}
