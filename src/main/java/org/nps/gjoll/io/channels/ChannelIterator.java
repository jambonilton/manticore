package org.nps.gjoll.io.channels;

import org.nps.gjoll.io.Deserializer;

import java.nio.channels.ByteChannel;
import java.util.Iterator;

public class ChannelIterator<E> implements Iterator<E> {

    final ByteChannel fc;
    final Deserializer<E> format;

    E obj;

    public ChannelIterator(ByteChannel fc, Deserializer<E> format) {
        this.fc = fc;
        this.format = format;
        advance();
    }

    private void advance() {
        obj = format.read(fc);
    }

    @Override
    public boolean hasNext() {
        return obj != null;
    }

    @Override
    public E next() {
        try {
            return obj;
        } finally {
            advance();
        }
    }

}
