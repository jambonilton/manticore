package org.nps.gjoll.io.channels;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.util.Arrays;

public class ChannelReader {

    final ByteChannel in;
    final ByteBuffer buffer;

    public ChannelReader(ByteChannel in) {
        this.in = in;
        this.buffer = Channels.getThreadBuffer();
    }

    public byte read() throws IOException {
        buffer.clear().limit(1);
        in.read(buffer);
        buffer.flip();
        if (buffer.hasRemaining())
            return buffer.get();
        return -1;
    }

    public ByteBuffer read(int nbytes) throws IOException {
        buffer.clear().limit(nbytes);
        in.read(buffer);
        buffer.flip();
        return buffer;
    }

    public String readSizedString() throws IOException {
        return readString(read());
    }

    public String readString(int nbytes) throws IOException {
        if (nbytes == -1)
            return null;
        read(nbytes);
        return new String(Arrays.copyOfRange(buffer.array(), buffer.position(), buffer.limit()));
    }

}
