package org.nps.gjoll.io.channels;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;

public class ChannelWriter {

    final ByteChannel channel;
    final ByteBuffer buffer;

    public ChannelWriter(ByteChannel channel) {
        this.channel = channel;
        this.buffer = Channels.getThreadBuffer();
    }

    public void put(byte b) throws IOException {
        buffer.put(b);
    }

    public void putShort(short s) throws IOException {
        buffer.putShort(s);
    }

    public void putChar(char c) throws IOException {
        buffer.putChar(c);
    }

    public void putInt(int i) throws IOException {
        buffer.putInt(i);
    }

    public void putLong(long i) throws IOException {
        buffer.putLong(i);
    }

    public void putFloat(float f) throws IOException {
        buffer.putFloat(f);
    }

    public void putDouble(double d) throws IOException {
        buffer.putDouble(d);
    }

    public void putBoolean(boolean b) throws IOException {
        buffer.put((byte)(b ? 0x01 : 0x00));
    }

    // TODO abstraction leak
    public void putString(String value) throws IOException {
        final byte[] bytes = value.getBytes();
        buffer.put((byte) bytes.length).put(bytes);
    }

    public void flush() throws IOException {
        buffer.flip();
        channel.write(buffer);
        buffer.clear();
    }

}
