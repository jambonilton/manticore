package org.nps.gjoll.io.channels;

import java.nio.ByteBuffer;

public class Channels {

    private static final int BYTE_BUFFER_SIZE = 24 * 1024; // TODO use system variable

    private static ThreadLocal<ByteBuffer> threadLocalBuffer = new ThreadLocal<ByteBuffer>() {
        @Override
        protected ByteBuffer initialValue() {
            return ByteBuffer.allocate(BYTE_BUFFER_SIZE);
        }
    };

    public static ByteBuffer getThreadBuffer() {
        final ByteBuffer buffer = threadLocalBuffer.get();
        buffer.clear();
        return buffer;
    }

}
