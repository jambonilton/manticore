package org.nps.gjoll.io.exec;

import org.nps.gjoll.Database;
import org.nps.gjoll.io.readers.PeekingReader;
import org.nps.gjoll.query.Query;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractDatabaseSerialExecution implements SerialExecution {

    final Database database;

    public AbstractDatabaseSerialExecution(Database database) {
        this.database = database;
    }

    protected Query readQuery() {
        return Query.alwaysTrue(); // TODO
    }

    protected List<String> readFields(PeekingReader reader) throws IOException {
        final List<String> fields = new ArrayList<>();
        fields.add(reader.readWord());
        for (int ch = reader.peek(); ch == ','; ch = reader.peek())
            fields.add(reader.skipIgnoreCase(",").readWord());
        return fields;
    }

}
