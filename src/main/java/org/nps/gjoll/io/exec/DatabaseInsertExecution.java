package org.nps.gjoll.io.exec;

import com.carrotsearch.hppc.ObjectIntMap;
import com.carrotsearch.hppc.ObjectIntOpenHashMap;
import org.nps.gjoll.Database;
import org.nps.gjoll.data.DTO;
import org.nps.gjoll.data.Streams;
import org.nps.gjoll.io.readers.PeekingReader;
import org.nps.gjoll.meta.ContentDescriptor;
import org.nps.gjoll.meta.FieldInfo;
import org.nps.gjoll.meta.TypelessDescriptor;
import org.nps.gjoll.statements.GenericLocator;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public class DatabaseInsertExecution extends AbstractDatabaseSerialExecution {

    public DatabaseInsertExecution(Database database) {
        super(database);
    }

    @Override
    public void execute(PeekingReader reader, BufferedWriter writer) throws IOException {

        reader.skipWhitespace().skipIgnoreCase("INSERT").skipWhitespace().skipIgnoreCase("INTO");

        final String table = reader.readWord();

        reader.skipWhitespace().skipIgnoreCase("(");

        final List<String> fields = readFields(reader);
        final ContentDescriptor<DTO> descriptor = new TypelessDescriptor(fields);
        final ObjectIntMap<String> fieldIndex = new ObjectIntOpenHashMap<>();
        for (int i=0; i < fields.size(); i++) {
            fieldIndex.put(fields.get(i), i);
        }

        reader.skipWhitespace()
              .skipIgnoreCase(")")
              .skipWhitespace()
              .skipIgnoreCase("VALUES")
              .skipWhitespace();

        final GenericLocator<DTO> locator = new GenericLocator<>(table, descriptor);

        database.get(locator).add(Streams.toStream(new RowIterator(fieldIndex, reader)));
    }

    private static class RowIterator implements Iterator<DTO> {

        final ObjectIntMap<String> fieldIndex;
        final PeekingReader reader;

        public RowIterator(ObjectIntMap<String> fieldIndex, PeekingReader reader) {
            this.fieldIndex = fieldIndex;
            this.reader = reader;
        }

        @Override
        public boolean hasNext() {
            try {
                return !reader.isAtEnd();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public DTO next() {
            try {
                return readRow();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        private DTO readRow() throws IOException {
            reader.skipWhitespace().skipIgnoreCase('(');
            final String[] values = new String[fieldIndex.size()];
            for (int i=0; i < values.length-1; i++)
                values[i] = readValue(reader, ',');
            values[values.length-1] = readValue(reader, ')');
            return new StringArrayDTO(fieldIndex, values);
        }

        private String readValue(PeekingReader reader, char expected) throws IOException {
            try {
                if (reader.peek() == '\'')
                    return reader.readUntil('\''); // TODO escaping
                else
                    return reader.readUntil(expected).trim();
            } finally {
                reader.skipIgnoreCase(expected);
            }
        }
    }

    static class StringArrayDTO implements DTO {

        final ObjectIntMap<String> fieldIndex;
        final String[] values;

        public StringArrayDTO(ObjectIntMap<String> fieldIndex, String[] values) {
            this.fieldIndex = fieldIndex;
            this.values = values;
        }

        @Override
        public Object get(String key) {
            if (!fieldIndex.containsKey(key))
                return null;
            return values[fieldIndex.get(key)];
        }

        @Override
        public Object get(FieldInfo key) {
            if (!fieldIndex.containsKey(key.getName()))
                return null;
            return values[fieldIndex.get(key.getName())]; // TODO convert to correct type
        }

        // TODO ?
        @Override
        public void set(FieldInfo field, Object value) {
            values[field.getIndex()] = String.valueOf(value);
        }
    }

}
