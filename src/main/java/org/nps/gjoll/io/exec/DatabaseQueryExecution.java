package org.nps.gjoll.io.exec;

import org.nps.gjoll.Database;
import org.nps.gjoll.data.DTO;
import org.nps.gjoll.data.Entries;
import org.nps.gjoll.io.readers.PeekingReader;
import org.nps.gjoll.meta.ContentDescriptor;
import org.nps.gjoll.meta.TypelessDescriptor;
import org.nps.gjoll.query.Query;
import org.nps.gjoll.query.SortOrder;
import org.nps.gjoll.query.Tautology;
import org.nps.gjoll.statements.GenericLocator;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.stream.Collectors;

public class DatabaseQueryExecution extends AbstractDatabaseSerialExecution {

    public static final Tautology NO_QUERY = Query.alwaysTrue();

    public DatabaseQueryExecution(Database database) {
        super(database);
    }

    @Override
    public void execute(PeekingReader reader, BufferedWriter writer) throws IOException {
        final ContentDescriptor<DTO> descriptor = new TypelessDescriptor(readFields(reader));
        reader.skipWhitespace().skipIgnoreCase("FROM");

        final String table = reader.readWord();
        String keyword = reader.readWord().toUpperCase();
        final Query query = keyword.equals("WHERE") ? readQuery() : NO_QUERY;
        if (NO_QUERY.equals(query))
            keyword = reader.readWord().toUpperCase();
        final Entries<SortOrder> sorting = keyword.equals("ORDER") ? readSorting(reader) : null;
        final GenericLocator<DTO> locator = new GenericLocator<>(table, descriptor);

        database.get(locator).get(query, sorting).forEach(object -> printRow(descriptor, writer, object));
    }

    private Entries<SortOrder> readSorting(PeekingReader reader) throws IOException {
        final Entries<SortOrder> sorting = new Entries<>();
        reader.skipWhitespace().skipIgnoreCase("BY");
        sorting.put(reader.readWord(), SortOrder.valueOf(reader.readWord().toUpperCase()));
        char ch = (char) reader.skipWhitespace().peek();
        while (ch == ',')
            sorting.put(reader.readWord(), SortOrder.valueOf(reader.readWord().toUpperCase()));
        return sorting;
    }

    private void printRow(ContentDescriptor<DTO> descriptor, BufferedWriter writer, DTO object) {
        try {
            writer.write(descriptor.stream()
                    .map(object::get)
                    .map(String::valueOf)
                    .collect(Collectors.joining(","))+"\n");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
