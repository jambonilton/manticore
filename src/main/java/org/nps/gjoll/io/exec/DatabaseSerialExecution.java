package org.nps.gjoll.io.exec;

import org.nps.gjoll.Database;
import org.nps.gjoll.io.readers.PeekingReader;

import java.io.BufferedWriter;
import java.io.IOException;

public class DatabaseSerialExecution implements SerialExecution {

    final DatabaseQueryExecution queryExecution;
    final DatabaseInsertExecution insertExecution;

    public DatabaseSerialExecution(Database database) {
        this.queryExecution = new DatabaseQueryExecution(database);
        this.insertExecution = new DatabaseInsertExecution(database);
    }

    @Override
    public void execute(PeekingReader reader, BufferedWriter writer) throws IOException {
        final int first = reader.peek();
        switch (first) {
            case 'S':
                queryExecution.execute(reader, writer);
                break;
            case 'I':
                insertExecution.execute(reader, writer);
                break;
            case 'U':
            case 'D':
            case 'L':
        }
    }

}
