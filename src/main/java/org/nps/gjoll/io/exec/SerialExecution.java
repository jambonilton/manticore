package org.nps.gjoll.io.exec;

import org.nps.gjoll.io.readers.PeekingReader;

import java.io.BufferedWriter;
import java.io.IOException;

public interface SerialExecution {

    void execute(PeekingReader reader, BufferedWriter writer) throws IOException;

}
