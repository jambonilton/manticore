package org.nps.gjoll.io.readers;

import java.io.IOException;

public interface PeekingReader {

    String read(int nchars) throws IOException;

    default String readWord() throws IOException {
        return skipWhitespace().readWhile(Character::isLetter);
    }

    default String readUntil(char until) throws IOException {
        return readWhile(c -> c != until);
    }

    String readWhile(CharacterPredicate p) throws IOException;

    int peek() throws IOException;

    int read() throws IOException;

    default PeekingReader skipIgnoreCase(char ch) throws IOException {
        return skipIgnoreCase(new String(new char[]{ch}));
    }

    PeekingReader skipIgnoreCase(String str) throws IOException;

    default PeekingReader skipWhitespace() throws IOException {
        return skipWhile(Character::isWhitespace);
    }

    PeekingReader skipWhile(CharacterPredicate p) throws IOException;

    boolean isAtEnd() throws IOException;

    @FunctionalInterface
    interface CharacterPredicate {
        boolean test(char ch);
    }
}
