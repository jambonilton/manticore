package org.nps.gjoll.io.readers;

import java.io.IOException;
import java.io.Reader;

public class SimplePeekingReader implements PeekingReader {

    public static final int EOF = -1, UNDEFINED = -2;
    private final Reader base;

    int peek = UNDEFINED;

    public SimplePeekingReader(Reader base) {
        this.base = base;
    }

    @Override
    public String read(int nchars) throws IOException {
        final char[] cbuf = new char[nchars];
        if (peek >= 0) {
            cbuf[0] = (char) peek;
            base.read(cbuf, 1, nchars-1);
        } else {
            base.read(cbuf);
        }
        peek = UNDEFINED;
        return new String(cbuf);
    }

    @Override
    public PeekingReader skipWhile(CharacterPredicate p) throws IOException {
        int ch;
        for (ch = read(); p.test((char) ch); ch = base.read())
            ;
        peek = ch;
        return this;
    }

    @Override
    public String readWhile(CharacterPredicate p) throws IOException {
        final StringBuilder sb = new StringBuilder();
        int ch;
        for (ch = read(); p.test((char) ch); ch = base.read())
            sb.append((char) ch);
        peek = ch;
        return sb.toString();
    }

    @Override
    public int peek() throws IOException {
        return peek > UNDEFINED ? peek : (peek = base.read());
    }

    @Override
    public int read() throws IOException {
        try {
            return peek > UNDEFINED ? peek : base.read();
        } finally {
            peek = UNDEFINED;
        }
    }

    @Override
    public PeekingReader skipIgnoreCase(String expected) throws IOException {
        final String actual = read(expected.length());
        if (!actual.equals(expected))
            throw new IllegalStateException("Expected \""+expected+"\" but was \""+actual+'"');
        return this;
    }

    @Override
    public boolean isAtEnd() throws IOException {
        return peek > EOF ? false : (peek = base.read()) == EOF;
    }
}
