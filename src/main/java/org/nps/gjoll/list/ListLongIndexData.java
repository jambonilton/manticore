package org.nps.gjoll.list;

import org.nps.gjoll.index.LongEntry;
import org.nps.gjoll.index.LongIndexData;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class ListLongIndexData<E> implements LongIndexData<E> {

    final List<E> list;

    public ListLongIndexData() {
        this(new ArrayList<>());
    }

    public ListLongIndexData(List<E> list) {
        this.list = list;
    }

    @Override
    public E get(long index) {
        return list.get((int) index);
    }

    @Override
    public synchronized long insert(E item) {
        final int size = list.size();
        list.add(item);
        return size;
    }

    @Override
    public Stream<E> elements() {
        return list.stream();
    }

    @Override
    public Stream<LongEntry<E>> entries() {
        return null; // TODO
    }
}
