package org.nps.gjoll.meta;

import java.lang.reflect.Field;

public class BaseFieldInfo {

    final BaseFieldInfo parent;
    final Field field;

    public BaseFieldInfo(BaseFieldInfo parent, Field field) {
        this.parent = parent;
        this.field = field;
    }

    public String getName() {
        return parent == null ? field.getName() : parent.getName() + '.' + field.getName();
    }

    public Class<?> getType() {
        return field.getType();
    }

    public Object get(Object object) {
        try {
            Object toGet = object;
            if (parent != null)
                toGet = parent.get(object);
            if (toGet == null)
                return null;
            return field.get(toGet);
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }
    }

    public void set(Object object, Object value) {
        try {
            Object toSet = object;
            if (parent != null) {
                toSet = parent.get(object);
                if (toSet == null) {
                    toSet = parent.getType().newInstance();
                    parent.set(object, toSet);
                }
            }
            field.set(toSet, value);
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }
    }
}
