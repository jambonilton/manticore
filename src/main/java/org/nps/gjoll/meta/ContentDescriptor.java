package org.nps.gjoll.meta;

import java.util.Map;

/**
 * Reference for describing desired fields for a selection.
 */
public interface ContentDescriptor<T> extends TableDescriptor {

    T create(Iterable<Map.Entry<FieldInfo, Object>> assignments);

    Iterable<Map.Entry<FieldInfo, Object>> iterate(T object);

}