package org.nps.gjoll.meta;

import org.nps.gjoll.data.ConversionUtil;
import org.nps.gjoll.data.Streams;
import org.nps.gjoll.query.Pair;

import java.util.Map;
import java.util.stream.Collectors;

public class ContentDescriptorAdapter<T> implements ContentDescriptor<T> {

    final ContentDescriptor<T> base;
    final TableDescriptor table;

    public ContentDescriptorAdapter(ContentDescriptor<T> base, TableDescriptor table) {
        this.base = base;
        this.table = table;
    }

    @Override
    public T create(Iterable<Map.Entry<FieldInfo, Object>> assignments) {
        Streams.toStream(assignments)
                .map(this::toBaseFormat)
                .filter(e -> e != null);
        return base.create(assignments);
    }

    // TODO efficiency? conversion?
    private Map.Entry<FieldInfo, Object> toBaseFormat(Map.Entry<FieldInfo, Object> entry) {
        final FieldInfo baseField = base.get(entry.getKey().getName());
        if (baseField == null)
            return null;
        return new Pair<>(baseField, ConversionUtil.convertTo(entry.getValue(), baseField.getType()));
    }

    @Override
    public Iterable<Map.Entry<FieldInfo, Object>> iterate(T object) {
        return Streams.toStream(base.iterate(object))
                .map(this::toTableFormat)
                .filter(e -> e != null)
                .collect(Collectors.toList());
    }

    private Map.Entry<FieldInfo, Object> toTableFormat(Map.Entry<FieldInfo, Object> entry) {
        final FieldInfo tableField = table.get(entry.getKey().getName());
        if (tableField == null)
            return null; // TODO throw here
        return new Pair<>(tableField, ConversionUtil.convertTo(entry.getValue(), tableField.getType()));
    }

    @Override
    public int getNumberOfFields() {
        return table.getNumberOfFields();
    }

    @Override
    public FieldInfo get(int index) {
        return table.get(index);
    }

    @Override
    public FieldInfo get(String fieldName) {
        return table.get(fieldName);
    }
}
