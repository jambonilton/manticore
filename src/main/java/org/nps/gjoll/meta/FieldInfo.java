package org.nps.gjoll.meta;

public interface FieldInfo {

    int getIndex();

    String getName();

    Class getType();

    Object get(Object arg);

    void set(Object arg, Object value);

}
