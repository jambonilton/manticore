package org.nps.gjoll.meta;

import org.nps.gjoll.data.ArrayDTO;
import org.nps.gjoll.data.DTO;
import org.nps.gjoll.query.Pair;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class FieldInfoDescriptor implements ContentDescriptor<DTO> {

    final List<FieldInfo> fields;

    public FieldInfoDescriptor(List<FieldInfo> fields) {
        this.fields = fields;
    }

    // TODO copy pasta
    @Override
    public DTO create(Iterable<Map.Entry<FieldInfo, Object>> assignments) {
        final Object[] values = new Object[getNumberOfFields()];
        assignments.forEach(e -> values[e.getKey().getIndex()] = e.getValue());
        return new ArrayDTO(this, values);
    }

    // TODO copy pasta
    @Override
    public Iterable<Map.Entry<FieldInfo, Object>> iterate(DTO dto) {
        return () -> new Iterator<Map.Entry<FieldInfo, Object>>() {
            int i=0;
            @Override
            public boolean hasNext() {
                return i < getNumberOfFields();
            }
            @Override
            public Map.Entry<FieldInfo, Object> next() {
                final FieldInfo key = get(i++);
                return new Pair<>(key, dto.get(key));
            }
        };
    }

    @Override
    public int getNumberOfFields() {
        return fields.size();
    }

    @Override
    public FieldInfo get(int index) {
        return fields.get(index);
    }

    // TODO slow
    @Override
    public FieldInfo get(String fieldName) {
        for (FieldInfo field : fields)
            if (fieldName.equals(field.getName()))
                return field;
        return null;
    }
}
