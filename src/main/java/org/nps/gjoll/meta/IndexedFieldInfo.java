package org.nps.gjoll.meta;

public class IndexedFieldInfo implements FieldInfo {

    final int index;
    final BaseFieldInfo field;

    public IndexedFieldInfo(int index, BaseFieldInfo field) {
        this.index = index;
        this.field = field;
    }

    @Override
    public int getIndex() {
        return index;
    }

    @Override
    public String getName() {
        return field.getName();
    }

    @Override
    public Class getType() {
        return field.getType();
    }

    @Override
    public String toString() {
        return field.getName() + '[' + index + ']';
    }

    @Override
    public void set(Object object, Object value) {
        field.set(object, value);
    }

    @Override
    public Object get(Object object) {
        return field.get(object);
    }

}
