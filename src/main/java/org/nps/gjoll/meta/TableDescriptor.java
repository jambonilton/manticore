package org.nps.gjoll.meta;

import org.nps.gjoll.data.Streams;

import java.util.Iterator;
import java.util.stream.Stream;

public interface TableDescriptor extends Iterable<FieldInfo> {

    int getNumberOfFields();

    FieldInfo get(int index);

    FieldInfo get(String fieldName);

    default Iterator<FieldInfo> iterator() {
        return new TableDescriptorIterator(this);
    }

    default Stream<FieldInfo> stream() {
        return Streams.toStream(iterator());
    }

    class TableDescriptorIterator implements Iterator<FieldInfo> {

        final TableDescriptor descriptor;
        int index = 0;

        public TableDescriptorIterator(TableDescriptor descriptor) {
            this.descriptor = descriptor;
        }

        @Override
        public boolean hasNext() {
            return index < descriptor.getNumberOfFields();
        }

        @Override
        public FieldInfo next() {
            return descriptor.get(index++);
        }

    }

}
