package org.nps.gjoll.meta;

import org.nps.gjoll.data.DataTypes;
import org.nps.gjoll.query.Pair;
import org.nps.gjoll.statements.ContentLocator;

import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Possible infinite loop when cycles in class structure.
 */
public class TypeContentDescriptor<T> implements ContentLocator<T> {

    final Class<T> type;
    final BaseFieldInfo[] fields;

    public TypeContentDescriptor(Class<T> type) {
        this.type = type;
        this.fields = getFields(type);
    }

    // Recursively looks through nested objects.
    private static BaseFieldInfo[] getFields(Class<?> type) {
        return Stream.of(type.getDeclaredFields())
                .flatMap(field -> getFields(null, field))
                .toArray(BaseFieldInfo[]::new);
    }

    private static Stream<BaseFieldInfo> getFields(BaseFieldInfo parent, Field field) {
        final BaseFieldInfo baseField = new BaseFieldInfo(parent, field);
        return DataTypes.isType(field.getType())
                ? Stream.of(baseField)
                : Stream.of(field.getType().getDeclaredFields()).flatMap(child -> getFields(baseField, child));
    }

    @Override
    public int getNumberOfFields() {
        return fields.length;
    }

    @Override
    public FieldInfo get(int index) {
        return new IndexedFieldInfo(index, fields[index]);
    }

    @Override
    public FieldInfo get(String fieldName) {
        for (int i = 0; i < fields.length; i++) {
            if (fields[i].getName().equals(fieldName))
                return new IndexedFieldInfo(i, fields[i]);
        }
        return null;
    }

    @Override
    public String getLabel() {
        return type.getSimpleName().toLowerCase();
    }

    @Override
    public T create(Iterable<Map.Entry<FieldInfo, Object>> assignments) {
        try {
            final T t = type.newInstance();
            for (Map.Entry<FieldInfo, Object> assignment : assignments) {
                fields[assignment.getKey().getIndex()].set(t, assignment.getValue());
            }
            return t;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Iterable<Map.Entry<FieldInfo, Object>> iterate(T t) {
        return () -> new FieldIterator(t);
    }

    class FieldIterator implements Iterator<Map.Entry<FieldInfo, Object>> {

        final T instance;

        int index = 0;


        public FieldIterator(T instance) {
            this.instance = instance;
        }

        @Override
        public boolean hasNext() {
            return index < fields.length;
        }

        @Override
        public Map.Entry<FieldInfo, Object> next() {
            try {
                final BaseFieldInfo field = fields[index];
                return new Pair<>(new IndexedFieldInfo(index++, field), field.get(instance));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }
}
