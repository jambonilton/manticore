package org.nps.gjoll.meta;

import org.nps.gjoll.data.ArrayDTO;
import org.nps.gjoll.data.DTO;
import org.nps.gjoll.query.Pair;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class TypelessDescriptor implements ContentDescriptor<DTO> {

    final List<String> fields;

    public TypelessDescriptor(List<String> fields) {
        this.fields = fields;
    }

    @Override
    public int getNumberOfFields() {
        return fields.size();
    }

    @Override
    public FieldInfo get(int index) {
        return new TypelessFieldInfo(index);
    }

    @Override
    public FieldInfo get(String fieldName) {
        for (int i=0; i < fields.size(); i++)
            if (fields.get(i).equals(fieldName))
                return get(i);
        return null;
    }

    @Override
    public Iterable<Map.Entry<FieldInfo, Object>> iterate(final DTO dto) {
        return () -> new Iterator<Map.Entry<FieldInfo, Object>>() {
            int i=0;
            @Override
            public boolean hasNext() {
                return i < getNumberOfFields();
            }
            @Override
            public Map.Entry<FieldInfo, Object> next() {
                final FieldInfo key = get(i++);
                return new Pair<>(key, dto.get(key));
            }
        };
    }

    @Override
    public DTO create(Iterable<Map.Entry<FieldInfo, Object>> assignments) {
        final Object[] values = new Object[getNumberOfFields()];
        assignments.forEach(e -> values[e.getKey().getIndex()] = e.getValue());
        return new ArrayDTO(this, values);
    }

    private class TypelessFieldInfo implements FieldInfo {

        private final int index;

        public TypelessFieldInfo(int index) {
            this.index = index;
        }

        @Override
        public int getIndex() {
            return index;
        }

        @Override
        public String getName() {
            return fields.get(index);
        }

        @Override
        public Class getType() {
            return String.class;
        }

        // TODO performance
        @Override
        public Object get(Object arg) {
            try {
                return arg.getClass().getDeclaredField(getName()).get(arg);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public void set(Object arg, Object value) {
            try {
                arg.getClass().getDeclaredField(getName()).set(arg, value);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public String toString() {
            return getIndex() + ":" + getName();
        }

    }
}
