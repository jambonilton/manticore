package org.nps.gjoll.net;

import org.nps.gjoll.io.exec.SerialExecution;
import org.nps.gjoll.io.readers.PeekingReader;
import org.nps.gjoll.io.readers.SimplePeekingReader;
import org.nps.gjoll.query.Query;
import org.nps.gjoll.query.Tautology;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class GenericSocketConsumer implements SocketConsumer {

    public static final Tautology NO_QUERY = Query.alwaysTrue();

    final SerialExecution execution;

    public GenericSocketConsumer(SerialExecution execution) {
        this.execution = execution;
    }

    @Override
    public void accept(Socket socket) throws IOException {
        final PeekingReader reader = new SimplePeekingReader(new InputStreamReader(socket.getInputStream()));
        final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        execution.execute(reader, writer);
    }

}
