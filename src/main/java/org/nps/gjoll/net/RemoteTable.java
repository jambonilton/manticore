package org.nps.gjoll.net;

import org.nps.gjoll.io.channels.ChannelIterator;
import org.nps.gjoll.io.Format;
import org.nps.gjoll.meta.FieldInfo;
import org.nps.gjoll.query.Query;
import org.nps.gjoll.query.SortOrder;
import org.nps.gjoll.meta.ContentDescriptor;
import org.nps.gjoll.statements.ContentLocator;
import org.nps.gjoll.tables.DataTable;

import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class RemoteTable<T> implements DataTable<T> {

    final ExecutorService executor;
    final ContentDescriptor<T> descriptor;
    final ContentLocator locator;
    final Format<T> format;
    final String host;
    final int port;

    public RemoteTable(ExecutorService executor,
                       ContentDescriptor<T> descriptor,
                       ContentLocator locator,
                       Format<T> format,
                       String host,
                       int port) {
        this.executor = executor;
        this.descriptor = descriptor;
        this.locator = locator;
        this.format = format;
        this.host = host;
        this.port = port;
    }

    @Override
    public void onUpdate(Query query, BiConsumer<T, Iterable<Map.Entry<String, Object>>> consumer) {
        // TODO
    }

    @Override
    public void onInsert(Query query, Consumer<T> consumer) {
        try {
            final Socket socket = getSocket();
            final ByteChannel channel = socket.getChannel();
            final String listenString = "LISTEN TO " + locator.getLabel()
                    + "(" + descriptor.stream().map(FieldInfo::getName).collect(Collectors.joining(", ")) + ") "
                    + "WHERE " + query
                    + "ON INSERT";
            channel.write(ByteBuffer.wrap(listenString.getBytes()));
            final Iterator<T> channelIterator = new ChannelIterator(channel, format);
            final Stream<T> stream = StreamSupport.stream(Spliterators.spliteratorUnknownSize(channelIterator,
                    Spliterator.ORDERED | Spliterator.NONNULL), false).onClose(()->closeChannel(channel));
            executor.execute(() -> stream.forEach(consumer::accept));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onDelete(Query query, Consumer<T> consumer) {
        try {
            final Socket socket = getSocket();
            final ByteChannel channel = socket.getChannel();
            final String listenString = "LISTEN TO " + locator.getLabel()
                    + "(" + descriptor.stream().map(FieldInfo::getName).collect(Collectors.joining(", ")) + ") "
                    + "WHERE " + query
                    + "ON DELETE";
            channel.write(ByteBuffer.wrap(listenString.getBytes()));
            final Iterator<T> channelIterator = new ChannelIterator(channel, format);
            final Stream<T> stream = StreamSupport.stream(Spliterators.spliteratorUnknownSize(channelIterator,
                    Spliterator.ORDERED | Spliterator.NONNULL), false).onClose(()->closeChannel(channel));
            executor.execute(() -> stream.forEach(consumer::accept));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void update(Query query, Collection<Map.Entry<String, Object>> assignments) {
        try (final Socket socket = getSocket()) {
            final ByteChannel channel = socket.getChannel();
            final String updateString = "UPDATE "+locator.getLabel()
                    +"SET "+assignments.stream().map(e -> e.getKey()+"="+e.getValue()).collect(Collectors.joining(", "))
                    +"VALUES "+query;
            channel.write(ByteBuffer.wrap(updateString.getBytes()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(Query query) {
        try (final Socket socket = getSocket()) {
            final ByteChannel channel = socket.getChannel();
            final String selectString = "DELETE "
                    + "FROM " + locator.getLabel()
                    + "WHERE " + query;
            channel.write(ByteBuffer.wrap(selectString.getBytes()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Stream<T> get(Query query, Iterable<Map.Entry<String, SortOrder>> sorting) {
        try (final Socket socket = getSocket()) {
            final ByteChannel channel = socket.getChannel();
            final String selectString = "SELECT "
                    + descriptor.stream().map(FieldInfo::getName).collect(Collectors.joining(", "))
                    + "FROM "+locator.getLabel()
                    + "WHERE " + query;
            channel.write(ByteBuffer.wrap(selectString.getBytes()));
            final Iterator<T> channelIterator = new ChannelIterator(channel, format);
            return StreamSupport.stream(Spliterators.spliteratorUnknownSize(channelIterator,
                    Spliterator.ORDERED | Spliterator.NONNULL), false).onClose(()->closeChannel(channel));
        } catch (IOException e) {
            throw new RuntimeException(e); // TODO
        }
    }

    @Override
    public void add(Stream<? extends T> items) {
        try (final Socket socket = getSocket()) {
            final ByteChannel channel = socket.getChannel();
            final String insertString = "INSERT INTO "
                    + locator.getLabel()
                    + "(" + descriptor.stream().map(FieldInfo::getName).collect(Collectors.joining(",")) + ")";
            channel.write(ByteBuffer.wrap(insertString.getBytes()));
            items.forEach(item -> format.write(channel, item));
        } catch (IOException e) {
            throw new RuntimeException(e); // TODO
        }
    }

    private void closeChannel(ByteChannel channel) {
        try {
            channel.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    // TODO reuse sockets
    private Socket getSocket() throws IOException {
        return new Socket(host, port);
    }

}
