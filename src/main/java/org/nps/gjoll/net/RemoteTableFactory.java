package org.nps.gjoll.net;

import org.nps.gjoll.statements.ContentLocator;
import org.nps.gjoll.tables.DataTable;
import org.nps.gjoll.tables.DataTableFactory;

import java.net.MalformedURLException;
import java.net.URL;

public class RemoteTableFactory implements DataTableFactory {

    private final URL url;

    public RemoteTableFactory(String url) throws MalformedURLException {
        this(new URL(url));
    }
    public RemoteTableFactory(URL url) {
        this.url = url;
    }

    @Override
    public <T> DataTable<T> get(ContentLocator<T> locator) {
        return null;
    }

}
