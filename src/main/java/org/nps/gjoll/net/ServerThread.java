package org.nps.gjoll.net;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Executor;

public class ServerThread implements Runnable {

    protected final Executor exec;
    protected final int port;
    protected final SocketConsumer socketConsumer;

    public ServerThread(Executor exec, int port, SocketConsumer socketConsumer) {
        this.exec = exec;
        this.port = port;
        this.socketConsumer = socketConsumer;
    }

    @Override
    public void run() {
        try (final ServerSocket socket = new ServerSocket(port)) {
            while (true)
                exec.execute(new SocketHandleThread(socket.accept()));
        } catch (Exception e) {
            throw new ServerRuntimeException(e);
        }
    }

    private final class SocketHandleThread implements Runnable {

        private final Socket connection;

        private SocketHandleThread(Socket connection) {
            this.connection = connection;
        }

        @Override
        public void run() {
            try {
                socketConsumer.accept(connection);
            } catch (Exception e) {
                throw new RuntimeException(e);
            } finally {
                try {
                    connection.close();
                } catch (IOException e) {
                    throw new ServerRuntimeException(e);
                }
            }
        }
    }

    public static class ServerRuntimeException extends RuntimeException {

        private static final long serialVersionUID = 1L;

        public ServerRuntimeException(Throwable cause) {
            super(cause);
        }

    }

}
