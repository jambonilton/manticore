package org.nps.gjoll.query;

import org.nps.gjoll.reflect.FieldExtractor;

public class BasicComparison extends Comparison<Object> {

    public BasicComparison(String k, Comparator c, Object o) {
        super(k, c, o);
    }

    @Override
    public boolean test(Object t) {
        return comparator.compare.apply(FieldExtractor.get(t, key), value);
    }
}
