package org.nps.gjoll.query;

import org.nps.gjoll.reflect.FieldExtractor;

public class IgnoreEmptyComparison extends BasicComparison {
    public <V> IgnoreEmptyComparison(Comparison<V> c) {
        super(c.getKey(), c.comparator(), c.getValue());
    }

    @Override
    public boolean test(Object t) {
        if (FieldExtractor.get(t, key) == null)
            return true;
        return super.test(t);
    }
}
