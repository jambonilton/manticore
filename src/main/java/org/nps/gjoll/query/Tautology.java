package org.nps.gjoll.query;

import java.util.Arrays;
import java.util.Iterator;

/**
 * Always true query.
 */
public class Tautology implements Query {

    @Override
    public boolean test(Object t) {
        return true;
    }

    @Override
    public Iterator<Query> iterator() {
        return Arrays.<Query>asList(this).iterator();
    }

    @Override
    public Query and(Query query) {
        return query;
    }

    @Override
    public Query or(Query query) {
        return this;
    }

    @Override
    public Query ignoreEmptyParameters() {
        return this;
    }
}
