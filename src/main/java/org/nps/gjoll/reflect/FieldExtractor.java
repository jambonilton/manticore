package org.nps.gjoll.reflect;

import static org.nps.gjoll.reflect.Reflector.reflect;

public class FieldExtractor {

    public static Object get(Object obj, String field) {
//        if (obj instanceof DTO)
//            return ((DTO) obj).get(field);
        return reflect(obj).get(obj, field);
    }

}
