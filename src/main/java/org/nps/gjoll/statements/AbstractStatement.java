package org.nps.gjoll.statements;

import org.nps.gjoll.Database;

public class AbstractStatement<T> {

    Database database;
    ContentLocator locator;

    public AbstractStatement(Database database, ContentLocator<T> locator) {
        this.database = database;
        this.locator = locator;
    }

    public ContentLocator from() {
        return locator;
    }
}
