package org.nps.gjoll.statements;

import org.nps.gjoll.meta.ContentDescriptor;

public interface ContentLocator<T> extends ContentDescriptor<T> {

    String getLabel();

}
