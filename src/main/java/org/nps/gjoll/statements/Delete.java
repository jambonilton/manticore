package org.nps.gjoll.statements;

import org.nps.gjoll.query.Query;

public interface Delete<T> {

    ContentLocator from();
    Delete<T> from(ContentLocator locator);

    void where(Query query);

}
