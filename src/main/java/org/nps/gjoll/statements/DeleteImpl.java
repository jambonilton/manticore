package org.nps.gjoll.statements;

import org.nps.gjoll.Database;
import org.nps.gjoll.query.Query;

public class DeleteImpl<T> extends AbstractStatement<T> implements Delete<T> {

    public DeleteImpl(Database database, ContentLocator<T> locator) {
        super(database, locator);
    }

    @Override
    public Delete<T> from(ContentLocator locator) {
        this.locator = locator;
        return this;
    }

    @Override
    public void where(Query query) {
        database.get(locator).delete(query);
    }

}
