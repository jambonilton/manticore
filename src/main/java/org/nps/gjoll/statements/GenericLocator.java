package org.nps.gjoll.statements;

import org.nps.gjoll.meta.ContentDescriptor;
import org.nps.gjoll.meta.FieldInfo;

import java.util.Map;

public class GenericLocator<T> implements ContentLocator<T> {

    final String table;
    final ContentDescriptor<T> descriptor;

    public GenericLocator(String table, ContentDescriptor<T> descriptor) {
        this.table = table;
        this.descriptor = descriptor;
    }

    @Override
    public String getLabel() {
        return table;
    }

    @Override
    public Iterable<Map.Entry<FieldInfo, Object>> iterate(T object) {
        return descriptor.iterate(object);
    }

    @Override
    public int getNumberOfFields() {
        return descriptor.getNumberOfFields();
    }

    @Override
    public FieldInfo get(int index) {
        return descriptor.get(index);
    }

    @Override
    public FieldInfo get(String fieldName) {
        return descriptor.get(fieldName);
    }

    @Override
    public T create(Iterable<Map.Entry<FieldInfo, Object>> assignments) {
        return descriptor.create(assignments);
    }
}
