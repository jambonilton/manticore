package org.nps.gjoll.statements;

import java.util.stream.Stream;

public interface Insert<T> {

    ContentLocator getLocator();

    Stream<? extends T> getValues();

    void execute();

}
