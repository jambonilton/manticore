package org.nps.gjoll.statements;

import org.nps.gjoll.Database;
import org.nps.gjoll.meta.ContentDescriptor;
import org.nps.gjoll.meta.TypeContentDescriptor;

import java.util.stream.Stream;

public class InsertImpl<T> extends AbstractStatement<T> implements Insert<T> {

    private final Stream<? extends T> values;

    public InsertImpl(Database database, Class<T> type, Stream<? extends T> values) {
        this(database, new TypeContentDescriptor<>(type), values);
    }
    public InsertImpl(Database database, ContentLocator<T> locator, Stream<? extends T> values) {
        super(database, locator);
        this.values = values;
    }

    @Override
    public Stream<? extends T> getValues() {
        return values;
    }

    @Override
    public ContentLocator getLocator() {
        return locator;
    }

    @Override
    public void execute() {
        database.get(locator).add(values);
    }
}
