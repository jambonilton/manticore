package org.nps.gjoll.statements;

import org.nps.gjoll.query.Query;

import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public interface Listen<T> {

    Listen<T> to(ContentLocator locator);

    Query where();
    Listen<T> where(Query query);

    Listen<T> onUpdate(BiConsumer<T, Iterable<Map.Entry<String, Object>>> consumer);

    Listen<T> onInsert(Consumer<T> consumer);

    Listen<T> onDelete(Consumer<T> consumer);

}
