package org.nps.gjoll.statements;

import org.nps.gjoll.Database;
import org.nps.gjoll.meta.ContentDescriptor;
import org.nps.gjoll.query.Query;

import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class ListenImpl<T> extends AbstractStatement<T> implements Listen<T> {

    Query query = Query.alwaysTrue();

    public ListenImpl(Database database, ContentLocator<T> locator) {
        super(database, locator);
    }

    @Override
    public Listen<T> to(ContentLocator locator) {
        this.locator = locator;
        return this;
    }

    @Override
    public Query where() {
        return query;
    }

    public Listen<T> where(Query query) {
        this.query = query;
        return this;
    }

    public Listen<T> onUpdate(BiConsumer<T, Iterable<Map.Entry<String, Object>>> consumer) {
        database.get(locator).onUpdate(query, consumer);
        return this;
    }

    public Listen<T> onInsert(Consumer<T> consumer) {
        database.get(locator).onInsert(query, consumer);
        return this;
    }

    public Listen<T> onDelete(Consumer<T> consumer) {
        database.get(locator).onDelete(query, consumer);
        return this;
    }

}
