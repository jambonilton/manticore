package org.nps.gjoll.statements;

import org.nps.gjoll.data.Entries;
import org.nps.gjoll.query.Query;
import org.nps.gjoll.query.SortOrder;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface Select<T> {

    ContentLocator from();
    Select<T> from(ContentLocator locator);

    Query where();
    Select<T> where(Query query);

    Entries<SortOrder> sorting();
    Select<T> sorting(Entries<SortOrder> sorting);

    Stream<T> stream();
    default T getFirst() {
        return findFirst().get();
    }
    default Optional<T> findFirst() {
        return stream().findFirst();
    }
    default List<T> list() {
        return stream().collect(Collectors.toList());
    }

}
