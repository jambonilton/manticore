package org.nps.gjoll.statements;

import org.nps.gjoll.Database;
import org.nps.gjoll.data.Entries;
import org.nps.gjoll.meta.ContentDescriptor;
import org.nps.gjoll.meta.TypeContentDescriptor;
import org.nps.gjoll.query.Query;
import org.nps.gjoll.query.SortOrder;

import java.util.stream.Stream;

public class SelectImpl<T> extends AbstractStatement<T> implements Select<T> {

    Query query;
    Entries<SortOrder> sorting;

    public SelectImpl(Database database, Class<T> type) {
        this(database, new TypeContentDescriptor<>(type));
    }

    public SelectImpl(Database database, ContentLocator<T> locator) {
        super(database, locator);
    }

    @Override
    public Query where() {
        return query;
    }

    @Override
    public Entries<SortOrder> sorting() {
        return sorting;
    }

    @Override
    public Select<T> where(Query query) {
        this.query = query;
        return this;
    }

    @Override
    public Select<T> sorting(Entries<SortOrder> sorting) {
        this.sorting = sorting;
        return this;
    }

    @Override
    public Select<T> from(ContentLocator locator) {
        this.locator = locator;
        return this;
    }

    @Override
    public Stream<T> stream() {
        return database.get(locator).get(query, sorting);
    }

}
