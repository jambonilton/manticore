package org.nps.gjoll.statements;

import org.nps.gjoll.data.Entries;
import org.nps.gjoll.query.Query;

public interface Update<T> {

    ContentLocator from();
    Update<T> from(ContentLocator locator);

    Query where();
    Update<T> where(Query query);

    Entries<Object> getAssignments();

    Update<T> setAssignments(Entries<Object> assignments);

    Update<T> set(String key, Object value);

    void execute();

}