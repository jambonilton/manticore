package org.nps.gjoll.statements;

import org.nps.gjoll.Database;
import org.nps.gjoll.data.Entries;
import org.nps.gjoll.meta.ContentDescriptor;
import org.nps.gjoll.meta.TypeContentDescriptor;
import org.nps.gjoll.query.Query;


public class UpdateImpl<T> extends AbstractStatement<T> implements Update<T> {

    Query query;
    Entries<Object> assignments = new Entries<>();

    public UpdateImpl(Database database, Class<T> type) {
        this(database, new TypeContentDescriptor<>(type));
    }

    public UpdateImpl(Database database, ContentLocator locator) {
        super(database, locator);
    }

    @Override
    public Update<T> from(ContentLocator locator) {
        this.locator = locator;
        return this;
    }

    @Override
    public Query where() {
        return query;
    }

    @Override
    public Update<T> where(Query query) {
        this.query = query;
        return this;
    }

    @Override
    public Entries<Object> getAssignments() {
        return assignments;
    }

    @Override
    public Update<T> setAssignments(Entries<Object> assignments) {
        this.assignments = assignments;
        return this;
    }

    @Override
    public Update<T> set(String key, Object value) {
        assignments.put(key, value);
        return this;
    }

    @Override
    public void execute() {
        this.database.get(locator).update(query, assignments);
    }

}
