package org.nps.gjoll.tables;

import org.nps.gjoll.statements.ContentLocator;

import java.util.HashMap;
import java.util.Map;

public class CachingTableFactory implements DataTableFactory {

    final DataTableFactory base;
    final Map<String, DataTable> cache;

    public CachingTableFactory(DataTableFactory base) {
        this.base = base;
        this.cache = new HashMap<>();
    }

    @Override
    public <T> DataTable<T> get(ContentLocator<T> locator) {
        final String key = locator.getLabel();
        DataTable<T> table = cache.get(key);
        if (table == null) {
            table = base.get(locator);
            cache.put(key, table);
        }
        return table;
    }

}
