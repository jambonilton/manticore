package org.nps.gjoll.tables;

import org.nps.gjoll.query.Query;

import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public interface DataTable<T> extends MutableDataTable<T> {

    void onUpdate(Query query, BiConsumer<T, Iterable<Map.Entry<String, Object>>> consumer);

    void onInsert(Query query, Consumer<T> consumer);

    void onDelete(Query query, Consumer<T> delete);

}
