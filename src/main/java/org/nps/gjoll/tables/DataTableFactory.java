package org.nps.gjoll.tables;

import org.nps.gjoll.statements.ContentLocator;

public interface DataTableFactory {

    <T> DataTable<T> get(ContentLocator<T> locator);

}
