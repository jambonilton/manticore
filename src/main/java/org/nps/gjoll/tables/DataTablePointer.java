package org.nps.gjoll.tables;

public interface DataTablePointer<T> {

    Class<T> getType();

    boolean hasName();

    String getName();

}