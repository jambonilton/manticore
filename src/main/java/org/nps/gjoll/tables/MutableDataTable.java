package org.nps.gjoll.tables;

import org.nps.gjoll.query.Query;

import java.util.Collection;
import java.util.Map;

public interface MutableDataTable<T> extends ReadOnlyDataTable<T> {

    void update(Query query, Collection<Map.Entry<String, Object>> assignments);

    void delete(Query query);

}
