package org.nps.gjoll.tables;

import org.nps.gjoll.query.Query;
import org.nps.gjoll.query.SortOrder;

import java.util.Map;
import java.util.stream.Stream;

public interface ReadOnlyDataTable<T> {

    Stream<T> get(Query query, Iterable<Map.Entry<String,SortOrder>> sorting);

    default Stream<T> get(Query query) {
        return get(query, null);
    }

    void add(Stream<? extends T> items);

    default void add(T item) {
        add(Stream.of(item));
    }

}
