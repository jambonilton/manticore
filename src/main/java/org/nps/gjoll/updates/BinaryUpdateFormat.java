package org.nps.gjoll.updates;

import org.nps.gjoll.meta.FieldInfo;
import org.nps.gjoll.meta.TableDescriptor;
import org.nps.gjoll.io.channels.ChannelReader;
import org.nps.gjoll.io.channels.ChannelWriter;
import org.nps.gjoll.io.Format;
import org.nps.gjoll.io.bson.BinaryObjectFormat;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.util.List;
import java.util.Map;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class BinaryUpdateFormat implements Format<IndexedUpdate> {

    final TableDescriptor tableInfo;

    public BinaryUpdateFormat(TableDescriptor tableInfo) {
        this.tableInfo = tableInfo;
    }

    @Override
    public IndexedUpdate read(ByteChannel in) {
        final ChannelReader reader = new ChannelReader(in);
        try {
            final List<Map.Entry<FieldInfo, Object>> assignments = readAssignments(reader);
            final ByteBuffer sizeBytes = reader.read(Integer.BYTES);
            if (sizeBytes.remaining() < Integer.BYTES)
                return null;
            final long[] indices = readIndices(in, sizeBytes.getInt());
            return new IndexedUpdate(assignments, indices);
        } catch (IOException | ReflectiveOperationException e) {
            throw new RuntimeException(e); // TODO
        }
    }

    private List<Map.Entry<FieldInfo, Object>> readAssignments(ChannelReader reader) throws IOException, ReflectiveOperationException {
        final int size = reader.read();
        final Spliterator<Map.Entry<FieldInfo, Object>> spliterator = Spliterators.spliterator(new BinaryObjectFormat.FieldValueIterator(tableInfo, size, reader), size, Spliterator.ORDERED);
        return StreamSupport.stream(spliterator,false).collect(Collectors.toList());
    }

    private long[] readIndices(ByteChannel in, int size) throws IOException {
        final long[] indices = new long[size];
        final ByteBuffer buffer = ByteBuffer.allocate(size * Long.BYTES);
        in.read(buffer);
        buffer.flip();
        for (int i=0; i < size; i++)
            indices[i] = buffer.getLong();
        return indices;
    }

    @Override
    public void write(ByteChannel out, IndexedUpdate changes) {
        final ChannelWriter writer = new ChannelWriter(out);
        try {
            writer.put((byte) changes.assignments.size());
            for (Map.Entry<FieldInfo, Object> assignment : changes.assignments) {
                writer.put((byte) assignment.getKey().getIndex());
                BinaryObjectFormat.writeValue(writer, assignment.getValue());
            }
            final long[] indices = changes.getIndices();
            writer.putInt(indices.length);
            for (int i=0; i < indices.length; i++)
                writer.putLong(indices[i]);
            writer.flush();
        } catch (IOException | ReflectiveOperationException e) {
            throw new RuntimeException(e); // TODO
        }
    }

}
