package org.nps.gjoll.updates;

import org.nps.gjoll.meta.FieldInfo;

import java.util.Collection;
import java.util.Map;

public class IndexedUpdate {

    final Collection<Map.Entry<FieldInfo, Object>> assignments;
    final long[] indices;

    public IndexedUpdate(Collection<Map.Entry<FieldInfo, Object>> assignments, long[] indices) {
        this.assignments = assignments;
        this.indices = indices;
    }

    public Collection<Map.Entry<FieldInfo, Object>> getAssignments() {
        return assignments;
    }

    public long[] getIndices() {
        return indices;
    }

    public void apply(Object obj) {
        for (Map.Entry<FieldInfo, Object> assignment : assignments)
            assignment.getKey().set(obj, assignment.getValue());
    }

}
