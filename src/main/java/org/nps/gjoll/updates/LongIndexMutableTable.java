package org.nps.gjoll.updates;

import org.nps.gjoll.meta.FieldInfo;
import org.nps.gjoll.meta.TableDescriptor;
import org.nps.gjoll.index.LongEntry;
import org.nps.gjoll.index.LongIndexDataTable;
import org.nps.gjoll.query.Pair;
import org.nps.gjoll.query.Query;
import org.nps.gjoll.query.SortOrder;
import org.nps.gjoll.tables.MutableDataTable;

import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LongIndexMutableTable<T> implements MutableDataTable<T> {

    final TableDescriptor tableInfo;
    final LongIndexDataTable<T> data;
    final LongIndexDataTable<IndexedUpdate> changes;

    public LongIndexMutableTable(TableDescriptor tableInfo, LongIndexDataTable<T> data, LongIndexDataTable<IndexedUpdate> changes) {
        this.tableInfo = tableInfo;
        this.data = data;
        this.changes = changes;
    }

    @Override
    public void update(Query query, Collection<Map.Entry<String, Object>> assignments) {
        final long[] matches = data.getEntries(query, null)
                .mapToLong(LongEntry::getKey)
                .toArray();
        final Collection<Map.Entry<FieldInfo, Object>> fieldAssignments = assignments.stream()
                .map(this::getFieldAssignment)
                .collect(Collectors.toList());

        changes.add(new IndexedUpdate(fieldAssignments, matches));
    }

    private Map.Entry<FieldInfo, Object> getFieldAssignment(Map.Entry<String, Object> assignment) {
        return new Pair<>(tableInfo.get(assignment.getKey()), assignment.getValue());
    }

    @Override
    public void delete(Query query) {
        // TODO
    }

    @Override
    public Stream<T> get(Query query, Iterable<Map.Entry<String, SortOrder>> sorting) {
        Stream<T> result = data.getEntries(null, null)
                .map(this::applyChanges);
        if (query != null)
            result = result.filter(query);
        return result;
    }

    private T applyChanges(LongEntry<T> entry) {
        changes.get(Query.contains("indices", entry.getKey()))
            .forEach(update -> update.apply(entry.getValue()));
        return entry.getValue();
    }

    @Override
    public void add(Stream<? extends T> items) {
        data.add(items);
    }

}
