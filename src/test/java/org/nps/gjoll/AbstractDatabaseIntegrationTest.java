package org.nps.gjoll;

import org.junit.Test;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.nps.gjoll.query.Query.*;

public abstract class AbstractDatabaseIntegrationTest {

    protected Database db;
    protected TestUser steve = new TestUser().setName("Steve Camuti").setAge(23).setSex('M').setId(1).setAddress(new TestAddress().setStreetNumber(10).setStreet("Main street"));
    protected TestUser bob = new TestUser().setName("Bob Loblaw").setAge(43).setSex('M').setId(2).setAddress(new TestAddress().setStreetNumber(16).setStreet("Loblaw lane"));
    protected TestUser john = new TestUser().setName("John Snow").setAge(18).setSex('M').setId(3);

    protected TestUser[] users = new TestUser[] {
        steve,
        bob,
        john
    };

    @Test
    public void singleItemTest() {
        db.add(steve);
        assertEquals(1, db.select(TestUser.class).stream().count());

        TestUser actual = db.select(TestUser.class).where(is("name", steve.name)).findFirst()
                .orElseThrow(AssertionError::new);
        assertEquals(steve.id, actual.id);
        assertEquals(steve.name, actual.name);
        assertEquals(steve.age, actual.age);

        final String newName = "Bob Loblaw";
        db.update(TestUser.class).set("name", newName).execute();
        actual = db.select(TestUser.class).where(is("name", newName)).findFirst()
                .orElseThrow(AssertionError::new);
        assertEquals(newName, actual.name);
    }

    @Test
    public void multiItemTest() {
        db.add(users);
        assertEquals(3, db.select(TestUser.class).stream().count());

        TestUser actual = db.select(TestUser.class).where(is("name", steve.name)).findFirst()
                .orElseThrow(AssertionError::new);
        assertEquals(steve.id, actual.id);
        assertEquals(steve.name, actual.name);
        assertEquals(steve.age, actual.age);

        final String newName = "Steve Buschemi";
        db.update(TestUser.class).set("name", newName).where(is("name", steve.name)).execute();
        actual = db.select(TestUser.class).where(contains("name", "Steve")).findFirst()
                .orElseThrow(AssertionError::new);
        assertEquals(newName, actual.name);
    }

    @Test
    public void rangeTest() {
        db.add(users);
        assertEquals(3, db.select(TestUser.class).stream().count());

        List<String> actual = db.select(TestUser.class).where(lessThan("age", 30)).stream()
                .map(TestUser::getName).collect(Collectors.toList());
        assertEquals(2, actual.size());
        assertTrue(actual.contains(steve.getName()));
        assertTrue(actual.contains(john.getName()));

        TestUser user = db.select(TestUser.class).where(greaterThan("name", "H").and(lessThan("name", "K")))
                .findFirst().orElseThrow(AssertionError::new);
        assertEquals(john.getName(), user.getName());
    }

    @Test
    public void triggersTest() {
        final EventHolder holder = new EventHolder();
        db.listen(TestUser.class).where(is("name", steve.name)).onInsert(u -> holder.setUser(u));
        db.add(steve);
        assertEquals(steve, holder.getUser().orElseThrow(AssertionError::new));
        holder.reset();
        db.listen(TestUser.class).onUpdate((u, pairs) -> holder.setUser(u));
        final String newName = "Steve Buschemi";
        db.update(TestUser.class).set("name", newName).where(is("name", steve.name)).execute();
        assertEquals(steve.id, holder.getUser().orElseThrow(AssertionError::new).id);
        TestUser actual = db.select(TestUser.class).where(contains("name", "Steve")).findFirst()
                .orElseThrow(AssertionError::new);
        assertEquals(newName, actual.name);
        db.listen(TestUser.class).onDelete(u -> holder.setUser(u));
        db.delete(TestUser.class).where(is("id", steve.id));
        assertEquals(steve.id, holder.getUser().orElseThrow(AssertionError::new).id);
    }

    public static class TestAddress {
        public Integer streetNumber;
        public String street;
        public Integer unitNumber;
        public String postalCode;

        public TestAddress setStreetNumber(Integer streetNumber) {
            this.streetNumber = streetNumber;
            return this;
        }

        public TestAddress setStreet(String street) {
            this.street = street;
            return this;
        }

        public TestAddress setUnitNumber(Integer unitNumber) {
            this.unitNumber = unitNumber;
            return this;
        }

        public TestAddress setPostalCode(String postalCode) {
            this.postalCode = postalCode;
            return this;
        }
    }

    public static class EventHolder {
        private TestUser user;
        public Optional<TestUser> getUser() {
            return Optional.ofNullable(user);
        }
        public void setUser(TestUser user) {
            this.user = user;
        }

        public void reset() {
            user = null;
        }
    }

}
