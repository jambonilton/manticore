package org.nps.gjoll;

public class TestUser {
    public int id;
    public Integer age;
    public String name;
    public Character sex;
    public AbstractDatabaseIntegrationTest.TestAddress address;

    public TestUser setId(int id) {
        this.id = id;
        return this;
    }

    public TestUser setAge(Integer age) {
        this.age = age;
        return this;
    }

    public TestUser setName(String name) {
        this.name = name;
        return this;
    }

    public TestUser setSex(Character sex) {
        this.sex = sex;
        return this;
    }

    public TestUser setAddress(AbstractDatabaseIntegrationTest.TestAddress address) {
        this.address = address;
        return this;
    }

    public int getId() {
        return id;
    }

    public Integer getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public Character getSex() {
        return sex;
    }

    public AbstractDatabaseIntegrationTest.TestAddress getAddress() {
        return address;
    }
}
