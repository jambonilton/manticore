package org.nps.gjoll.file;

import org.junit.After;
import org.junit.Before;
import org.nps.gjoll.AbstractDatabaseIntegrationTest;
import org.nps.gjoll.Database;
import org.nps.gjoll.DatabaseSettings;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileDatabaseIntegrationTest extends AbstractDatabaseIntegrationTest {

    Path dir = Paths.get("testdb");

    @Before
    public void setUp() {
        db = Database.init(new DatabaseSettings().setPath("testdb"));
    }

    @After
    public void tearDown() {
        deleteDirectory(dir.toFile());
    }

    /**
     * DFS traversal of directory, deleting all files.
     */
    public static boolean deleteDirectory(File directory) {
        if (!directory.exists())
            return true;
        final File[] files = directory.listFiles();
        if (files == null)
            return false;
        for(int i=0; i < files.length; i++) {
            if(files[i].isDirectory())
                deleteDirectory(files[i]);
            else
                files[i].delete();
        }
        return directory.delete();
    }

}
