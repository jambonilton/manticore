package org.nps.gjoll.file;

import org.junit.Before;
import org.junit.Test;
import org.nps.gjoll.io.SmallStringFormat;

import java.io.File;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class FileIndexedDataTest {

    String[] strings = {"one", "two", "three"};

    FileLongIndexData<String> data;

    @Before
    public void setup() throws Exception {
        data = new FileLongIndexData<>(File.createTempFile("test", "data"), new SmallStringFormat());
    }

    @Test
    public void addGet() {
        long[] indices = new long[3];
        for (int i=0; i < strings.length; i++)
            indices[i] = data.insert(strings[i]);
        for (int i=0; i < indices.length; i++)
            assertEquals(strings[i], data.get(indices[i]));
        final Object[] actual = data.elements().toArray();
        assertArrayEquals(strings, actual);
    }

}