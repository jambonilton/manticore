package org.nps.gjoll.io.bson;

import org.junit.Before;
import org.junit.Test;
import org.nps.gjoll.meta.TypeContentDescriptor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

import static org.junit.Assert.assertEquals;

public class BinaryObjectFormatTest {

    BinaryObjectFormat<User> format;

    File file;

    @Before
    public void setup() throws Exception {
        this.file = File.createTempFile("test", "bson");
        this.format = new BinaryObjectFormat(new TypeContentDescriptor<>(User.class));
    }

    @Test
    public void writeAndRead() throws Exception {
        final User user = new User();
        {
            user.id = 1;
            user.name = "Steve";
            user.sex = 'M';
        }
        try (FileChannel channel = new FileOutputStream(file).getChannel()) {
            format.write(channel, user);
        }
        try (FileChannel channel = new FileInputStream(file).getChannel()) {
            User dto = format.read(channel);
            assertEquals(user.id, dto.id);
            assertEquals(user.name, dto.name);
            assertEquals(user.age, dto.age);
            assertEquals(user.sex, dto.sex);
        }
    }

    public static class User {
        public int id;
        public Integer age;
        public String name;
        public Character sex;
    }

}
