package org.nps.gjoll.io.bson;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

import static org.junit.Assert.assertArrayEquals;

public class FieldDescriptorFormatTest {

    FieldDescriptor[] fields = new FieldDescriptor[]{
            new FieldDescriptor("test", String.class),
            new FieldDescriptor("foo", Long.class),
            new FieldDescriptor("bar", Double.class)
    };
    FieldDescriptorFormat format = new FieldDescriptorFormat();

    File file;

    @Before
    public void setup() throws Exception {
        file = File.createTempFile("test", "fields");
    }

    @Test
    public void writeAndReads() throws Exception {
        try (FileChannel channel = new FileOutputStream(file).getChannel()) {
            for (FieldDescriptor field : fields)
                format.write(channel, field);
        }
        FieldDescriptor[] actual = new FieldDescriptor[fields.length];
        try (FileChannel channel = new FileInputStream(file).getChannel()) {
            for (int i=0; i < actual.length; i++)
                actual[i] = format.read(channel);
        }
        assertArrayEquals(fields, actual);
    }

}