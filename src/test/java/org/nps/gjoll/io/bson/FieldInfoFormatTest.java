package org.nps.gjoll.io.bson;

import org.junit.Before;
import org.junit.Test;
import org.nps.gjoll.TestUser;
import org.nps.gjoll.meta.FieldInfo;
import org.nps.gjoll.meta.TypeContentDescriptor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

import static org.junit.Assert.assertEquals;

public class FieldInfoFormatTest {

    FieldInfoFormat format = new FieldInfoFormat();

    File file;

    @Before
    public void setup() throws Exception {
        file = File.createTempFile("test", "fields");
    }

    @Test
    public void writeAndReads() throws Exception {
        final TypeContentDescriptor<TestUser> descriptor = new TypeContentDescriptor<>(TestUser.class);
        try (FileChannel channel = new FileOutputStream(file).getChannel()) {
            for (FieldInfo field : descriptor)
                format.write(channel, field);
        }
        try (FileChannel channel = new FileInputStream(file).getChannel()) {
            for (int i=0; i < descriptor.getNumberOfFields(); i++) {
                final FieldInfo expected = descriptor.get(i);
                final FieldInfo actual = format.read(channel);
                assertEquals(expected.getIndex(), actual.getIndex());
                assertEquals(expected.getName(), actual.getName());
                assertEquals(expected.getType(), actual.getType());
            }
        }
    }

}
