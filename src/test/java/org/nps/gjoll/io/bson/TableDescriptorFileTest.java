package org.nps.gjoll.io.bson;

import org.junit.Test;
import org.nps.gjoll.TestUser;
import org.nps.gjoll.meta.ContentDescriptor;
import org.nps.gjoll.meta.FieldInfo;
import org.nps.gjoll.meta.TableDescriptor;
import org.nps.gjoll.meta.TypeContentDescriptor;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class TableDescriptorFileTest {

    @Test
    public void readsAndWrites() throws IOException {
        final TableDescriptorFile io = new TableDescriptorFile(new FieldInfoFormat());
        final File file = File.createTempFile("test", "meta");
        final ContentDescriptor<TestUser> testDescriptor = new TypeContentDescriptor<>(TestUser.class);
        io.write(file, testDescriptor);
        final TableDescriptor descriptor = io.read(file);
        for (FieldInfo expected : testDescriptor) {
            FieldInfo actual = descriptor.get(expected.getIndex());
            assertEquals(expected.getIndex(), actual.getIndex());
            assertEquals(expected.getName(), actual.getName());
            assertEquals(expected.getType(), actual.getType());
        }
    }

}