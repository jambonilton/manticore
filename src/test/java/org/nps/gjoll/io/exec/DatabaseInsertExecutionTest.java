package org.nps.gjoll.io.exec;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.nps.gjoll.Database;
import org.nps.gjoll.DatabaseSettings;
import org.nps.gjoll.TestUser;
import org.nps.gjoll.io.readers.PeekingReader;
import org.nps.gjoll.io.readers.SimplePeekingReader;

import java.io.*;

import static org.junit.Assert.assertEquals;

public class DatabaseInsertExecutionTest {

    public static final String DB_PATH = "insert-test";

    Database db;

    @Before
    public void setUp() {
        db = Database.init(new DatabaseSettings().setPath(DB_PATH));
    }

//    @After
//    public void tearDown() {
//        deleteDirectory(dir.toFile());
//    }

    @Test
    public void test() throws Exception {
        db.add(new TestUser().setId(0).setName("Bob")); // primes the database with correct columns
        DatabaseInsertExecution insert = new DatabaseInsertExecution(db);
        final InputStreamReader baseReader = new InputStreamReader(new ByteArrayInputStream("INSERT INTO TestUser (id, name, age) VALUES (1, 'Steve', 30)".getBytes()));
        try {
            PeekingReader reader = new SimplePeekingReader(baseReader);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out));
            insert.execute(reader, writer);
            assertEquals(1, db.select(TestUser.class).stream().skip(1).findFirst().get().id);
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder();
            int ch;
            while (-1 != (ch = baseReader.read()))
                sb.append((char) ch);
            System.out.println(sb.toString());
            throw e;
        }
    }

    @After
    public void tearDown() {
        deleteDirectory(new File(DB_PATH));
    }

    /**
     * DFS traversal of directory, deleting all files.
     */
    public static boolean deleteDirectory(File directory) {
        if (!directory.exists())
            return true;
        final File[] files = directory.listFiles();
        if (files == null)
            return false;
        for(int i=0; i < files.length; i++) {
            if(files[i].isDirectory())
                deleteDirectory(files[i]);
            else
                files[i].delete();
        }
        return directory.delete();
    }

}