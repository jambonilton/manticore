package org.nps.gjoll.list;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class ListIndexedDataTest {


    String[] strings = {"one", "two", "three"};

    ListLongIndexData<String> data;

    @Before
    public void setup() throws Exception {
        data = new ListLongIndexData<>();
    }

    @Test
    public void addGet() {
        long[] indices = new long[3];
        for (int i=0; i < strings.length; i++)
            indices[i] = data.insert(strings[i]);
        for (int i=0; i < indices.length; i++)
            assertEquals(strings[i], data.get(indices[i]));
        final Object[] actual = data.elements().toArray();
        assertArrayEquals(strings, actual);
    }

}